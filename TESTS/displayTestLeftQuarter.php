<!DOCTYPE html>
<html>
<head>
	<title>Template One</title>

	<style type="text/css">
		html,
		body {
		  height: 100vh;
		  width: 100vw;
		  overflow: hidden;
		}

		.my-container {
		  display: flex;
		  flex-direction: column;
		  height: 100vh;
		  width:100vw;
		}

		.my-container>.top [class^="col-"],
		.my-container>.bottom [class^="col-"] {
		  background-color: #778899  ;
		  color: white;
		  text-align: center;
		}

		.my-container>.middle {
		  flex-grow: 1;
		  padding:30px;
		  background-image: url('images/bg_rainbow.svg');
		  background-size: cover;
		}

		.my-container>.middle>* {
		}

		#clock{
		    /*background-color:#333;*/
		    font-family: sans-serif;
		    font-size:40px;
		    text-shadow:0px 0px 1px #fff;
		    color:#fff;
		}
		#clock span {
		    color:#fff;
		    font-size:40px;
		    position:relative;

		}
		#date {
			margin-top: -10px;
		    letter-spacing:3px;
		    font-size:20px;
		    font-family:arial,sans-serif;
		    color:#fff;
		}
	</style>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" crossorigin="anonymous">
	<link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	     
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
	<script type='text/javascript' src='//cdn.jsdelivr.net/jquery.marquee/1.4.0/jquery.marquee.min.js'></script>

</head>
<body onload="startTime()">
<div class="container-fluid my-container">
	<div class="row top">
		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-3">
					<img style="height: 100px;width: 250px;" src="/images/jacksonLogoGRN.svg">
				</div>
				<div class="col-lg-6">
					<a class="weatherwidget-io" href="https://forecast7.com/en/35d16n84d88/cleveland/?unit=us" data-label_1="CLEVELAND" data-label_2="WEATHER" data-days="3" data-theme="original" data-basecolor="" >CLEVELAND WEATHER</a>
				</div>
				<div class="col-lg-3 align-self-center">
					<div id="clockdate">
						<div class="clockdate-wrapper">
							<div id="clock"></div>
							<div id="date"></div>
						</div>
					</div>			
				</div>
			</div>
		</div>
	</div>

	<?php 
	$leftContent = "Testing Left Widget";
	$rightContent = 'images/pennington.jpg';
	$youtube = 'https://www.youtube.com/embed/Lh9hA_3S8l4?autoplay=1';	
	$room = 'images/room.jpg';	 
	?>
	

	<div class="row middle">
	    <div class="col-lg-6"  >
	      <div class="row" style="height:50%; padding-bottom: 15px;">
	      	<div class="col-lg-12" style="height:100%;">
	      		<div style="height: 100%;"><iframe width="100%" height="100%" src="<?php echo $youtube ?>"  allow="autoplay; encrypted-media"></iframe></div>
	      	</div>
	      </div>
	      <div class="row j" style="height:50%; padding-top: 15px;">
	      	<div class="col-lg-12 text-center col-lg-offset-2" style="height:100%; background-color: white; display:flex; align-items: center; text-align: center;">
	     		
	      		<img  src="images/sofa1.jpg" style="width: 50%; padding-left:20px; padding-right:20px;">
	      		<img src="images/sofa2.jpg" style="width: 50%; padding-left:20px; padding-right:20px;">

	      	</div>
	      </div>
	    </div>
	    <div class="col-lg-6" >
	      	<div style="background-color: rgba(255, 255, 255, 0.5); height: 100%; text-align: center; padding:10px;">
	      		<h2>Announcement</h2>
	      		<hr>

	      		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse in nulla tellus. In rutrum faucibus arcu, nec venenatis metus laoreet a. Mauris varius vulputate neque id fermentum. Nunc vitae odio nec purus pulvinar rhoncus dictum sit amet dui. Proin in hendrerit sem. Fusce id sollicitudin urna. In ornare lacus vel sagittis ornare. Nam ultrices condimentum felis ac maximus. Donec semper aliquam dui eget faucibus. Duis venenatis pulvinar sapien, tempor volutpat nulla ultricies non.</p>

				<p>Integer maximus arcu quis quam eleifend efficitur. Phasellus luctus venenatis ultricies. Suspendisse ac tincidunt ante. In ut enim commodo, ullamcorper turpis sed, ullamcorper nisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec efficitur neque malesuada metus sagittis sagittis. Nullam pellentesque enim vel sollicitudin mollis. Maecenas a ante convallis, rhoncus nibh in, ultrices mauris. Aliquam sit amet ligula pellentesque, sollicitudin tellus quis, blandit risus. Quisque et congue lorem, non viverra turpis. Praesent eget vulputate libero, eu viverra arcu. Nam ac iaculis justo. Sed nec pellentesque eros.</p>
			<hr>
				<p>Integer maximus arcu quis quam eleifend efficitur. Phasellus luctus venenatis ultricies. Suspendisse ac tincidunt ante. In ut enim commodo, ullamcorper turpis sed, ullamcorper nisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec efficitur neque malesuada metus sagittis sagittis. Nullam pellentesque enim vel sollicitudin mollis. Maecenas a ante convallis, rhoncus nibh in, ultrices mauris. Aliquam sit amet ligula pellentesque, sollicitudin tellus quis, blandit risus. Quisque et congue lorem, non viverra turpis. Praesent eget vulputate libero, eu viverra arcu. Nam ac iaculis justo. Sed nec pellentesque eros.</p>

				<ul style="text-align: left;">
					<li>Donec dapibus velit nec diam fermentum ullamcorper.</li>
					<li>Mauris sollicitudin leo id est condimentum viverra.</li>
					<li>Suspendisse venenatis purus et pellentesque bibendum.</li>
					<li>Donec dapibus velit nec diam fermentum ullamcorper.</li>
					<li>Mauris sollicitudin leo id est condimentum viverra.</li>
					<li>Suspendisse venenatis purus et pellentesque bibendum.</li>
				</ul>


			</div>
	    </div>
	</div>

	<div class="row bottom">
		<div class="col-lg-12">
			<div class="marquee"><h2 style="letter-spacing: 3px;">Happy Birthday/Work Anniversary!<span style=" color: #A9BD50; margin-right: 80px; margin-left: 80px;">|</span>	 Congratulations to {name} for winning last week's sales competition!<span style=" color: #A9BD50; margin-right: 80px; margin-left: 80px;">|</span>Weather Notice:</h2></div>
		</div>
	</div>
</div>


<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>

 <script src="js/clock.js"></script>

 <script type="text/javascript">
 	$('.marquee').marquee({
 		duration: 30000,
 		gap:30,
 	});
 </script>


</body>
</html>