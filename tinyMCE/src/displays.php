<!DOCTYPE html>
<html>
<head>
  <style type="text/css">
    @import "global.css";
  </style>
</head>

<body>
	<div>
		<img src="/images/jacksonLogoGRN.svg" height="100" width="250" />
		<h1 style="text-align: center; color: #A9BD50; ">Choose A Template</h1>
	</div>


<!--Template 1-->
<div class="templateLeft">
  <h3 style="text-align:center">Template 1</h3>
  <div class="pageRectangle">
    <div class="fiftyPercentRectangleLeft"><p style="text-align: center">50% Left Widget</p></div>
    <div class="fiftyPercentRectangleRight"><p style="text-align: center">50% Right Widget</p></div>
    <div class="ticker"><p style="text-align: center">Ticker</p></div>
  </div>
</div>

<!--Template 2-->
<div class="templateRight">
  <h3 style="text-align:center">Template 2</h3>
  <div class="pageRectangle">
  </div>
</div>


<!--Template 1-->
<div class="templateLeft">
  <h3 style="text-align:center">Template 3</h3>
  <div class="pageRectangle">
  </div>
</div>

<!--Template 2-->
<div class="templateRight">
  <h3 style="text-align:center">Template 4</h3>
  <div class="pageRectangle">
  </div>
</div>

</body>
</html>