<!DOCTYPE html>
<html>
<head>
	<title>Menu 2</title>
	<!-- Bootstrap CSS CDN -->
<!--     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
 -->    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style.css">
    <!--Font Awesome CDN-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>
<body>
<header>
    <div class="branding">
        <div class="menu-button menu-toggle nav">
            <i class="material-icons">menu</i>
        </div>
        <img src="images/jacksonLogoGRN.svg"  />
    </div>
    <div class="page-details">
    </div>
    <div class="settings">
        <div class="menu-button profile">
            <i class="far fa-user-circle fa-2x"></i>
        </div>
        <div class="menu-button menu-toggle aside">
            <i class="material-icons">chat</i>
        </div>
    </div>
</header>
<div class="app">
    <nav>
        <div class="title-block">
            <img src="images/jacksonLogoGRN.svg" />
        </div>
        <ul>
            <li><a href="#"><i class="material-icons">home</i>
                <span>Home</span></a></li>
            <li><a href="#"><i class="material-icons">adb</i>
                <span>Menu Item with a Long Name</span></a></li>
            <li><a href="#"><i class="material-icons">android</i>
                <span>Android</span></a></li>
            <li><a href="#"><i class="material-icons">attachment</i>
                <span>Attachments</span></a></li>
            <li><a href="#"><i class="material-icons">bookmark</i>
                <span>Bookmarks</span></a></li>
            <li><a href="#"><i class="material-icons">star</i>
                <span>Favorites</span></a></li>
            <li><a href="#"><i class="material-icons">build</i>
                <span>Configuration</span></a></li>
            <li><a href="#"><i class="material-icons">cake</i>
                <span>Birthday Party</span></a></li>
            <li><a href="#"><i class="material-icons">brush</i>
                <span>Designer</span></a></li>
            <li><a href="#"><i class="material-icons">camera</i>
                <span>Photos</span></a></li>
        </ul>
    </nav>
<!--     <article>
        <div class="card"></div>
        <div class="card"></div>
        <div class="card"></div>
        <div class="card"></div>
        <div class="card"></div>
        <div class="card"></div>
        <div class="card"></div>
        <div class="card"></div>
        <div class="card"></div>
        <div class="card"></div>
    </article> -->
</div>

<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<!-- Bootstrap Js CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $(".menu-toggle").on("click", function(e) {
        if($(this).hasClass("nav")) {
            $("nav").addClass("open");
        }
        else {
            $("aside").addClass("open");
        }
        e.stopPropagation();
    });
    
    $("body:not(nav)").on("click", function(e) {
        $("nav, aside").removeClass("open");
    });
});
</script>
<script>
   window.FontAwesomeConfig = {
      searchPseudoElements: true
   }
</script>
</body>
</html>

