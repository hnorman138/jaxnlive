<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>Global Nav Menu</title>

	<!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="global.css">
    <!--Font Awesome CDN-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>
<body>
	<nav id="sidebar">
		<!-- Nav Sidebar Header -->
		<div class="sidebar-header">
			<h3><img class="logo" src="images/jacksonLogoGRN.svg" height="50%" width="80%"/></h3>
			<strong>J L</strong>
		</div>

		<!-- nav links -->
		<ul class="list-unstyled components">
			<li>
				<a href="index.php">
					<i class="fas fa-tachometer-alt"></i>
					Dashboard
				</a>
			</li>
			<li class="active">
				<a href= "#homeSubmenu" data-toggle="collapse" aria-expanded="false">
					<i class="fas fa-tv"></i>
					Displays
				</a>
				<ul class="collapse list-unstyled" id="homeSubmenu">
                    <li><a href="#">Add Display</a></li>
                    <li><a href="#">Edit Display</a></li>
                </ul>
			</li>
			<li class="active">
				<a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">
					<i class="far fa-file-alt"></i>
					Pages
				</a>
				<ul class="collapse list-unstyled" id="pageSubmenu">
                    <li><a href="#">Add Page</a></li>
                    <li><a href="#">Edit Page</a></li>
                </ul>
			</li>
			<li class="active">
				<a href="#panelSubmenu" data-toggle="collapse" aria-expanded="false">
					<i class="fas fa-columns"></i>
					Panels
				</a>
				<ul class="collapse list-unstyled" id="panelSubmenu">
                    <li><a href="#">Add Page</a></li>
                    <li><a href="#">Edit Page</a></li>
                </ul>
			</li>
			<li>
				<a href="content.php">
					<i class="far fa-folder-open"></i>
					Content
				</a>
			</li>
		</ul>
		<div class="socialWrapper">
			<ul class="list-unstyled social">
	            <li><a href="https://www.facebook.com/BuiltByJackson/" target="_blank" class="fab fa-facebook-square fa-2x"></a></li>
	            <li><a href="#" target="_blank" class="fab fa-instagram fa-2x"></a></li>
	            <li><a href="https://twitter.com/BuiltByJackson" target="_blank" class="fab fa-twitter fa-2x"></a></li>
	            <li><a href="https://www.pinterest.com/jacksonstyle/" target="_blank" class="fab fa-pinterest fa-2x"></a></li>
	        </ul>
        </div>
	</nav>
<!-- 	<div style="position: sticky; width:100%" class="mt-3">
		<div style="position: sticky; flex-grow: 2;" class="d-xl-inline-flex flex-row mb-3 bg-secondary">
			<button type="button" id="sidebarCollapse" class=".btn" >
				<span></span>
				<span></span>
				<span></span>
			</button>
			<div class="p-2">Test 1</div>
			<div class="p-2 ml-auto">Test 2</div>
		</div>
	</div>
 -->
<div id="secondHeader">
		<div id="userHeader">
			<button type="button" id="sidebarCollapse" class=".btn" >
				<span></span>
				<span></span>
				<span></span>
			</button>
			<i id="user" class="far fa-user-circle fa-2x"></i>
		</div>
	</div>

<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<!-- Bootstrap Js CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<script type="text/javascript">
$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
        $(this).toggleClass('active');
    });
});
</script>

<script>
   window.FontAwesomeConfig = {
      searchPseudoElements: true
   }
</script>
</body>
</html>