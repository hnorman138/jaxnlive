<!DOCTYPE html>
<html>
<head>
	<title>Template One</title>

	<style type="text/css">
		@import "style.css";
		html,
		body {
		  height: 100%;
		  overflow: hidden;
		}

		.my-container {
		  display: flex;
		  flex-direction: column;
		  height: 100%;
		}

		.my-container>.top [class^="col-"],
		.my-container>.bottom [class^="col-"] {
		  background-color: #929292  ;
		  color: white;
		  text-align: center;
		}

		.my-container>.middle {
		  flex-grow: 1;
		  padding:30px;
		  /*background-image: url('images/bg_orange.svg');*/
		  background-size: cover;
		}

		.my-container>.middle>* {
		}

		#clock{
		    /*background-color:#333;*/
		    font-family: sans-serif;
		    font-size:40px;
		    text-shadow:0px 0px 1px #fff;
		    color:#fff;
		}
		#clock span {
		    color:#fff;
		    font-size:40px;
		    position:relative;

		}
		#date {
			margin-top: -10px;
		    letter-spacing:3px;
		    font-size:20px;
		    font-family:arial,sans-serif;
		    color:#fff;
		}
	</style>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" crossorigin="anonymous">
	<link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	     
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
	<script type='text/javascript' src='//cdn.jsdelivr.net/jquery.marquee/1.4.0/jquery.marquee.min.js'></script>

</head>
<body onload="startTime()">

<?php include("menu.php");?>

<?php startblock('article') ?>
	<!-- TITLE BLOCK -->
	<div class="container-fluid title-container">
		<div class="row">
			<div class="col-lg-12" style="text-align: center;">
				<div class="page-header">
	        	<h2>Create A New Full-Width Page</h2>      
	      		</div>
	      	</div>
      	</div>
      	<div class="row">
      		<div class="col-lg-3"><a href="#exampleModal" data-toggle="modal" data-target="#exampleModal">Choose Page Background</a></div>
      	</div>
      	<br>
	</div>
	<hr>

	<?php
	$images = array('images/bg_orange.svg','images/bg_green.svg','images/bg_ocean.svg', 'images/bg_blueGreen.svg','images/bg_rainbow.svg', 'images/bg_spring.svg');	
	?>

	<!-- MAIN TEMPLATE CONTAINER -->
	<div class="container-fluid my-container">

		<!-- Background image modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h5 class="modal-title" id="exampleModalLabel">Choose an image:</h5>
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	          </button>
	        </div>
	        <div class="modal-body">

	        	<?php foreach($images as $im): ?>
		        <img style="width: 200px; height: 200px;" src="<?php echo $im ?>">
		    	<?php endforeach ?>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary">Save changes</button>
		      </div>
		    </div>
		  </div>
		</div>

	  <div class="row top">
	    <div class="col-lg-12">
	    	<div class="row">
	    		<div class="col-lg-3">
	    			<img style="height: 100px;width: 250px;" src="/images/jacksonLogoGRN.svg">
	    		</div>
	    		<div class="col-lg-6">
	    		<a class="weatherwidget-io" href="https://forecast7.com/en/35d16n84d88/cleveland/?unit=us" data-label_1="CLEVELAND" data-label_2="WEATHER" data-days="3" data-theme="original" data-basecolor="" >CLEVELAND WEATHER</a>
		    	</div>
	    		<div class="col-lg-3 align-self-center">
		    		<div id="clockdate">
					  <div class="clockdate-wrapper">
					    <div id="clock"></div>
					    <div id="date"></div>
					  </div>
					</div>			
	    		</div>
	    	</div>
	    </div>
	  </div>
	  <div class="row middle">
	    <div class="col-lg-12">
	    	<div style="border: 1px dotted black; background-color: white; height: 100%;">Full-Width</div>
	    </div>
	  </div>
	  <div class="row bottom">
	    <div class="col-lg-12">
	      <div class="marquee"><h2>This is a test</h2></div>
	    </div>
	  </div>
	</div>

	<div class="row" style="padding-top: 20px;">
		<div class="col-lg-12">
		<button style="float: left;">Preview</button>
	<button style="float:right;" data-toggle="modal" data-target="#savePageModal">Save</button>
	<!-- Save Page Modal -->
		<div class="modal fade" id="savePageModal" tabindex="-1" role="dialog" aria-labelledby="savePageLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h5 class="modal-title" id="savePageLabel">Page Details:</h5>
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	          </button> 
	        </div>
	        <div class="modal-body">
	        	<div class="form-group">
	        		<label for="plantSelect">Select A Location</label>
	        		<select class="form-control" id="plantSelect">
	        			<?php foreach($displays["Company"] as $area_name => $area_details): ;?>
	        			<option><?php echo $area_name ?></option>
	        			<?php endforeach ?>
	        		</select>

	        		<label for="areaSelect">Select An Area</label>
	        		<select class="form-control" id="areaSelect">
	        			<?php foreach($displays["Company"] as $area_name => $area_details): ;?>
	        			<option>Lobby</option>
	        			<?php endforeach ?>
	        		</select>

	        		<label for="durationSet">Set A Duration (in seconds)</label>
	        		<input class="form-control" id="durationSet">

	        		</input>

	        		<label for="expirationSelect">Set Expiration Date/Time</label>
	        		<select class="form-control" id="expirationSelect">
	        			
	        			<option>12/07/2018 13:45:00</option>
	        			
	        		</select>


	        	</div>
		    </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary">Save changes</button>
		      </div>
		    </div>
		  </div>
		</div>
		</div>
		</div>
	</div>


<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>

 <script src="js/clock.js"></script>

 <script type="text/javascript">
 	$('.marquee').marquee({
 		duration: 30000,
 		gap:30,
 	});
 </script>

 <script type="text/javascript">

$('#exampleModal .modal-body img').click(function() {
  const src = $(this).attr("src");
  $('.my-container > .middle').css("background-image", `url(${src})`);
  jQuery.noConflict();
  $('#exampleModal').modal('hide');
});

 </script>

<?php endblock() ?>

</body>
</html>