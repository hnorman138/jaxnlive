<!DOCTYPE html>
<html>
<head>
	<title></title>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet"/>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://rawgit.com/tempusdominus/bootstrap-4/master/build/js/tempusdominus-bootstrap-4.min.js"></script>
</head>
<body>


<?php include 'menu.php';  ?>
<form action="addPage.php" method="post">
	<div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-labelledby="savePageLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h5 class="modal-title" id="savePageLabel">Page Details:</h5>
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	          </button> 
	        </div>
	        <div class="modal-body">
	        	<div class="form-group">
	        		
	        		<input type="hidden" name="pageType" value="<?php echo $value;?>">
	        		<input type="hidden" name="panelType" value="<?php echo $panelType;?>">
	        		<!-- <input type="hidden" name="fullText" value> -->
	        		<input type="hidden" id="new" name="page_content[]" value="">
	        		<!-- <p id="demo"></p> -->
	        		</br>
	        		<label for="addTitle">Page Title:</label>
	        		<input class="form-control" id="addTitle" name="addTitle">

	        		</input>

	        		<!-- <label for="plantSelect">Select A Location</label>
	        		<select name="" class="form-control" id="plantSelect">
	        			<?php //foreach($displayNames as $key => $displayName):?>
						<option value=""><?php //echo $key; ?></option>
						<?php //endforeach;?>
	        		</select> -->

	        		<label for="areaSelect">Select An Area</label>
	        		<select name="displayId" class="form-control" id="areaSelect">
	        			<option value="" disabled selected>Select one...</option>
	        			<optgroup label="Plant 1">
	        			<option value="1">Lobby</option>
	        			<option value="2">Break Room</option>
	        			<optgroup label="Plant 2">
	        			<option value="3">Lobby</option>
	        			<option value="4">Break Room</option>
	        			<optgroup label="Chattanooga Office">
	        			<option value="5">Main Conference Room</option>
	        			<option value="6">Common Area</option>
	        			<optgroup label="Plant 4">
	        			<option value="7">Break Room</option>
	        			<option value="8">Lobby</option>
	        			<option value="9">Line 2</option>
	        			<optgroup label="Plant 5">
	        			<option value="10">Break Room</option>
	        			<option value="11">Lobby</option>
	        			<option value="12">Line 1</option>	
	        		</select>

	        		<label for="durationSet">Set A Duration (in seconds)</label>
	        		<input class="form-control" id="durationSet" name="durationSet">

	        		</input>

	        		<label for="expirationSelect">Set Expiration Date/Time</label>
	        		<div class="form-group">
                      <div class="datepick input-group date" id="datetimepicker" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker" name="datePicker" />
                        <span class="input-group-addon" data-target="#datetimepicker" data-toggle="datetimepicker">
                        <span class="fa fa-calendar"></span>
                        </span>
                      </div>
                    </div>
	        	</div>
		    </div>
		      <div class="modal-footer">
		        <input type="submit" name="Save Page">
		      </div>
		    </div>
		  </div>
		</div>
	</div>
	</form>


<script type="text/javascript"> 
$(window).on('load',function(){
        $('#my_modal').modal('show');
    });
        </script>





</body>
</html>