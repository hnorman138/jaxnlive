<?php include 'getPages.php';?>

<!DOCTYPE html>
<html>
<head>

    <style type="text/css" media="all">
  @import "style.css";
  </style>

  <!--Bootstrap CSS-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>

<body>

<?php include("menu.php");?>
<?php startblock('article') ?>
<!-- CONTAINER -->
<div class="container-fluid" style="margin-bottom: 20px;">
	<div class="row" style="width: 100%; padding-bottom: 20px;">
    <div class="col-sm-12">
      <div class="page-header">
        <h2>Displays</h2>  
        <hr>    
      </div>
      <div class="row">
		<?php foreach($displayNames as $key => $displayName):?>
        <div class="col-lg-3 col-sm-6 d-flex" style="padding-bottom: 20px;">
          <div style="padding-bottom:20px;" class="card text-center flex-fill">
            <h4 class="card-title"><?php echo $key ?></h4>
            <?php foreach($displayName as $key2 => $displayNameRoom): ?>
            <a href="displayPage.php?displayID=<?php echo $key2;?>"><p class="card-text"><?php echo $displayNameRoom ?></p></a>
            <?php endforeach; ?>
          </div>
        </div>
      <?php endforeach; ?>
      </div>
    </div>
    <!-- END FIRST ROW -->
  </div>


			

</div>

<?php endblock() ?>
</body>
</html>