<?php

/*This file is included in every template content modal so I'm creating an iterator and incrementing for the event that there are multiple modals on the template (any template other than full width*/

if( !isset($n_id) ) 
     $n_id = 0;
?>

<ul class="nav nav-tabs card-header-tabs">
	<li class="nav-item">
	  <a data-toggle="tab" class="nav-link active" href="#images<?php echo $n_id?>"><h3>Images</h3></a>
	</li>
	<li class="nav-item">
	  <a data-toggle="tab" class="nav-link" href="#widgets<?php echo $n_id?>"><h3>Widgets</h3></a>
	</li>
</ul>

<div class="tab-content">
	  <div role="tabpanel" id="images<?php echo $n_id?>" class="tab-pane fade show active">
      <div class="w3-content w3-display-container" style="text-align: center;">
        <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
  		  <?php foreach ($imageResult as $im): ?>
      		<?php if($im['type'] == 'content'){?>
            
        	  <img class="mySlides" src="<?php echo $im['url']; ?>" style="max-width:150px; height:150px;">

       	  <?php } ?>
       	<?php endforeach?>
        <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
      </div>
     	<p>Drag an image into the 'Create your own content' box below</p>
  	</div>
  	<div role="tabpanel" id="widgets<?php echo $n_id?>" class="tab-pane fade">
  		
  		<div class="row">
  			<div class="col-lg-12" style="margin-top:10px;">
  			<div class="card-deck">
  				<div class="card h-100">
  					<div class="card-body text-center">
  						<h4>Pinfo Line Stats</h4>
  						<div class="form-group">
	  						<label for="pinfoPlant" style="float: left;">Choose plant: </label>
	  						<select class="form-control" id="pinfoPlant">
	  							<option>Plant 1</option>
	  							<option>Plant 3</option>
	  							<option>Plant 4</option>
	  							<option>Plant 5</option>
	  							<option>Plant 7</option>
	  							<option>Plant 8</option>
	  							<option>Plant 9</option>
	  							<option>Kayline</option>
	  						</select>
	  					</div>
  					</div>
  				</div>

  				<div class="card h-100">
  					<div class="card-body text-center">
  						<h4>Social Media</h4>
  						<div class="form-group">
  							<label for="socialMediaSelect" style="float: left;">Select Service:</label>
  							<select class ="form-control" id="socialMediaSelect">
  								<option>Instagram</option>
  								<option>Twitter</option>
  							</select>
  						</div>
  					</div>
  				</div>

  				<div class="card h-100">
  					<div class="card-body text-center">
  						<h4>Third Widget</h4>
  					</div>
  				</div>
  			</div><!--end card deck-->
  		</div>
  		</div><!--end row-->
  	</div><!--end widgets-->
</div><!--end tabs-->

<?php $n_id++;?>