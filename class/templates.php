<?php include('getPages.php');?>

<!DOCTYPE html>
<html>
<head>
	<title>Template One</title>

	<style type="text/css">
		@import "style.css";
		html,
		body {
		  height: 100%;
		  overflow: hidden;
		}

		.my-container {
		  display: flex;
		  flex-direction: column;
		  height: 100%;
		}

		.my-container>.top [class^="col-"],
		.my-container>.bottom [class^="col-"] {
		  background-color: #929292  ;
		  color: white;
		  text-align: center;
		}

		.my-container>.middle {
		  flex-grow: 1;
		  padding:30px;
		  background-image: url('images/bg6.jpg');
		  background-size: cover;
		}

		.my-container>.middle>* {
		}

		#clock{
		    /*background-color:#333;*/
		    font-family: sans-serif;
		    font-size:40px;
		    text-shadow:0px 0px 1px #fff;
		    color:#fff;
		}
		#clock span {
		    color:#fff;
		    font-size:40px;
		    position:relative;

		}
		#date {
			margin-top: -10px;
		    letter-spacing:3px;
		    font-size:20px;
		    font-family:arial,sans-serif;
		    color:#fff;
		}

		.buttonContainer {
		  display: flex;
		  flex-direction: column;
		  justify-content: center;
		}
	</style>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <link href="https://rawgit.com/tempusdominus/bootstrap-4/master/build/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="src/ytv.css" type="text/css" rel="stylesheet" />

  

    
  <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="instafeed.js-master/instafeed.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.0/moment-with-locales.js"></script>
  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="https://rawgit.com/tempusdominus/bootstrap-4/master/build/js/tempusdominus-bootstrap-4.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
  <script src="src/ytv.js" type="text/javascript"></script>
  	<script type='text/javascript' src='//cdn.jsdelivr.net/jquery.marquee/1.4.0/jquery.marquee.min.js'></script>


</head>
<body onload="startTime()">

<?php include("menu.php");?>

<?php startblock('article') ?>

	<!-- TITLE BLOCK -->
	<div class="container-fluid title-container">
		<div class="row">
			<div class="col-lg-12" style="text-align: center;">
				<div class="page-header">
	        	<h2>Create A New Page</h2>      
	      		</div>
	      	</div>
      	</div>
      	<div class="row">
      		<div class="col-lg-3"><a href="#exampleModal" data-toggle="modal" data-target="#exampleModal">Choose Page Background</a></div>
      	</div>
      	<br>
	</div>
	<hr>

	<?php
	$images = array('images/bg_orange.svg','images/bg_green.svg','images/bg_ocean.svg', 'images/bg_blueGreen.svg','images/bg_rainbow.svg', 'images/bg_spring.svg');
	$bodyImg = array('images/chair.jpg', 'images/room.jpg');

	$displays = array(
	  "Company" => array(
	    "Plant 1"=>array(
	      "Displays"=>array(
	        "Lobby",
	        "Break Room",
	        "Line",
	        ),
	      ),
	    "Plant 2"=>array(
	      "Displays"=>array(
	        "Break Room",
	        "Line",
	        ),
	      ),
	    "Plant 3"=>array(
	      "Displays"=>array(
	        "Lobby",
	        "Break Room",
	        ),
	      ),
	    "Plant 4"=>array(
	      "Displays"=>array(
	        "Lobby",
	        "Break Room",
	        "Line"
	      ),
	    ),
	  ),
	);

	?>

	<div class="container-fluid my-container">

		<!-- Background image modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h5 class="modal-title" id="exampleModalLabel">Choose an image:</h5>
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	          </button>
	        </div>
	        <div class="modal-body">

	        	<?php foreach($images as $im): ?>
		        <img style="width: 200px; height: 200px;" src="<?php echo $im ?>">
		    	<?php endforeach ?>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary">Save changes</button>
		      </div>
		    </div>
		  </div>
		</div>


	  	<?php $value = isset($_GET['value']) ? $_GET['value'] : 1;?>
	  	<!-- Full width -->
	  	<?php if($value == 1){?>
	  	<?php include 'class/fullWidth.php'?>
	    
	    <!-- quarter-right -->
	    <?php }elseif($value == 3 || $value == 4){ ?>
	    <?php include 'class/quarterwidth.php' ?>
	    
	    <!--Half Width Template-->
	    <?php }else{?>
	    <?php include 'class/halfWidth.php'?>
		<?php } ?>


	  <!-- <div class="row bottom">
	    <div class="col-lg-12">
	      <div class="marquee"><h2>This is a test</h2></div>
	    </div>
	  </div> -->
	</div>
	<div>
	<button style="float: left;">Preview</button>
	<button style="float:right;" data-toggle="modal" data-target="#savePageModal">Save</button>
	<!-- Save Page Modal -->
	<div class="modal fade" id="savePageModal" tabindex="-1" role="dialog" aria-labelledby="savePageLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h5 class="modal-title" id="savePageLabel">Page Details:</h5>
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	          </button> 
	        </div>
	        <div class="modal-body">
	        	<form action="addPage.php" method="post">
	        	<div class="form-group">
	        		<input type="hidden" name="pageType" value="<?php echo $value;?>">
	        		</br>
	        		<label for="addTitle">Page Title:</label>
	        		<input class="form-control" id="addTitle" name="addTitle">

	        		</input>

	        		<label for="plantSelect">Select A Location</label>
	        		<select name="" class="form-control" id="plantSelect">
	        			<?php foreach($displayNames as $key => $displayName):?>
						<option value=""><?php echo $key; ?></option>
						<?php endforeach;?>
	        		</select>

	        		<label for="areaSelect">Select An Area</label>
	        		<select name="displayId" class="form-control" id="areaSelect">

	        			<!-- <option value=""></option> -->
	        			<option value="">1</option>
	        			<option value="">2</option>
	        			<option value="">3</option>
	        			
	        		</select>

	        		<label for="durationSet">Set A Duration (in seconds)</label>
	        		<input class="form-control" id="durationSet" name="durationSet">

	        		</input>

	        		<label for="expirationSelect">Set Expiration Date/Time</label>
	        		<div class="form-group">
                      <div class="datepick input-group date" id="datetimepicker" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker" name="datePicker" />
                        <span class="input-group-addon" data-target="#datetimepicker" data-toggle="datetimepicker">
                        <span class="fa fa-calendar"></span>
                        </span>
                      </div>
                    </div>
	        	</div>
	        </form>
		    </div>
		      <div class="modal-footer">
		        <input type="submit" name="Save Page">
		      </div>
		    </div>
		  </div>
		</div>
	</div>

<!-- <script type="text/javascript">

		var Company = {
	  "Building 1": [{
	    "Displays": [
	      "Lobby",
	      "Break Room",
	      "Office"
	    ]
	  }],
	  "Building 2": [{
	    "Displays": [
	      "Break Room",
	      "Office"
	    ]
	  }],
	  "Building 3": [{
	    "Displays": [
	      "Lobby",
	      "Break Room"
	    ]
	  }],
	  "Building 4": [{
	    "Displays": [
	      "Lobby",
	      "Break Room",
	      "Office"
	    ]
	  }]
	};

	$.each(Company, function(bld) {
  $("#plantSelect").append('<option value="' + bld + '">' + bld + '</option>')
});
$("#plantSelect").on("change", function() {
  $("#areaSelect")[0].length=1;
  if (this.value) {
    $.each(Company[this.value][0].Displays,function(_,disp) {
      $("#areaSelect").append('<option value="' + disp + '">' + disp + '</option>')
    });
  }  
});
</script> -->

<script src="tinyMCE/js/tinymce/tinymce.min.js"></script>
  <script type="text/javascript">
  tinymce.init({
    selector: '#mytextarea',
	height: 500,
  theme: 'modern',
  plugins: 'print preview fullpage paste searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools media  link contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
  });
  </script>
    <script type="text/javascript">
  tinymce.init({
    selector: '#mytextarea2',
	height: 500,
  theme: 'modern',
  plugins: 'print preview fullpage paste searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools media  link contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
  });
  </script>

      <script type="text/javascript">
  tinymce.init({
    selector: '#mytextarea3',
	height: 500,
  theme: 'modern',
  plugins: 'print preview fullpage paste searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools media  link contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
  });
  </script>

<!-- <script type="text/javascript">
    $(document).ready(function(){

    $("#form-data").submit(function(e){

        var content = tinymce.get("mytextarea").getContent();

        $(".leftContent").html(content);

        jQuery.noConflict();

        $('#leftFiftyModal').modal('hide');
        $('.modal-backdrop').remove();

        return false;

    });

});
</script>
 -->
<!-- <script type="text/javascript">
    $(document).ready(function(){

    $("#form-data2").submit(function(e){

        var content2 = tinymce.get("mytextarea2").getContent();

        $(".rightContent").html(content2);

        jQuery.noConflict();

        $('#rightFiftyModal').modal('hide');
        $('.modal-backdrop').remove();

        return false;

    });

});
</script> -->

<!-- <script type="text/javascript">
    $(document).ready(function(){

    $("#form-data3").submit(function(e){

        var content3 = tinymce.get("mytextarea3").getContent();

        $(".fullContent").html(content3);

        jQuery.noConflict();

        $('#fullModal').modal('hide');
        $('.modal-backdrop').remove();

        return false;

    });

});
</script> -->


<script type="text/javascript">
$(function () {
    $('.datepick').each(function(){
      $(this).datetimepicker({
       icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
    });
    });
});
</script>

<script type="text/javascript">
var $modalLeft = $('#leftFiftyModal').modal({
    show: false
});
$('.leftFifty').on('click', function() {
    $modalLeft.modal('show');
});

var $modalRight = $('#rightFiftyModal').modal({
    show: false
});
$('.rightFifty').on('click', function() {
    $modalRight.modal('show');
});

var $modalFull = $('#fullModal').modal({
    show: false
});
$('.fullWidth').on('click', function() {
    $modalFull.modal('show');
});

</script>

<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>

 <script src="js/clock.js"></script>

 <script type="text/javascript">
 	$('.marquee').marquee({
 		duration: 30000,
 		gap:30,
 	});
 </script>

<script type="text/javascript">
$('#exampleModal .modal-body img').click(function() {
  const src = $(this).attr("src");
  $('.my-container > .middle').css("background-image", `url(${src})`);
  jQuery.noConflict();
  $('#exampleModal').modal('hide');
});

 </script>

<?php endblock() ?>

</body>
</html>