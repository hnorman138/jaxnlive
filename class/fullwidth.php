<?php include '../getpages.php'?>

<div class="row top">
	<?php include 'banner.php'?>
</div>

<div style="text-align: center; margin-top: 15px;">
	<p>(Content must fit within bounds of dotted border)</p>
</div>

<div class="row middle" id="background">
	<form><input type="hidden" name="panel" value="background"></form>
	<div class="col-lg-12 fullWidth" id="full">
		<form><input type="hidden" name="panelFull" value="full"></form>
		<div class="fullContent"  id="fullContent" style="background-color:white; border: dotted 1px black;">
			
		</div>

	</div>
</div>

<div class="modal fade bd-example-modal-lg" id="fullModal" tabindex="-1" role="dialog" aria-labelledby="fullLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      	<div class="modal-content">
	        <div class="modal-header">         
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	          </button>
	        </div>
	        <div class="modal-body">
	        <?php include 'widgets.php'; ?>
			<hr>
	        	
	        	<h3>Create your own content</h3>
	        	<form  id="fullForm" method="post">
	        	  <input type="hidden" name="page_content" id="fullPageContent" value="">
	        	  <input type="hidden" name="panel_type" value="1">
			      <textarea class="original" id="fullTextArea" name="fullText"></textarea>
			      <input type="submit" value="Save Content">
			    </form>
		    </div>
		    <div class="modal-footer">
		      <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button> -->
		    </div>
	   </div>
	</div>
</div>

<div class="row bottom">
<div class="col-lg-12">
	<div class="marquee"><h2>Ticker</h2></div>
</div>
</div>
