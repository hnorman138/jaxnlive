<?php include '../getpages.php'?>


<div class="row top">
	<?php include 'banner.php'?>
</div>

<div style="text-align: center; margin-top: 15px;">
  <p>(Content must fit within bounds of dotted border)</p>
</div>

<div class="row middle">
<!--if left or right-->
<?php $value = isset($_GET['value']) ? $_GET['value'] : 1;?>
<?php if ($value == 4){ ?>

<!-- right -->
<div class="col-lg-6 leftFifty" id="leftFifty">
  	<div class="leftContent" id="leftContent" style="background-color: white; border: dotted 1px black;"></div>
</div>
<div class="col-lg-6"  >
  <div class="row" style="height:50%; padding-bottom: 15px;">
  	<div class="col-lg-12 topRight" id="topRightQuarter" style="height:100%;">
  		<div class="topRightContent" id="topRightContent" style="background-color: white; border: dotted 1px black;"></div>
  	</div>
  </div>
  <div class="row" style="height:50%; padding-top: 15px;">
  	<div class="col-lg-12 bottomRight" id="bottomRightQuarter" style="height:100%;">
  		<div class="bottomRightContent" id="bottomRightContent" style="background-color: white; border: dotted 1px black;"></div>
  	</div>
  </div>
</div>
<?php } elseif ($value == 3){ ?>

<!-- Left -->
<div class="col-lg-6"  >
  <div class="row" style="height:50%; padding-bottom: 15px;">
  	<div class="col-lg-12 topLeft" id="topLeftQuarter" style="height:100%;">
  		<div class="topLeftContent" id="topLeftContent" style="background-color: white; border: dotted 1px black;"></div>
  	</div>
  </div>
  <div class="row" style="height:50%; padding-top: 15px;">
  	<div class="col-lg-12 bottomLeft" id="bottomLeftQuarter" style="height:100%;">
  		<div class="bottomLeftContent" id="bottomLeftContent" style="background-color: white; border: dotted 1px black;"></div>
  	</div>
  </div>
</div>
<div class="col-lg-6 rightFifty" id="rightFifty">
  	<div class="rightContent" id="rightContent" style="background-color: white; border: dotted 1px black;"></div>
</div>
<?php } ?>
</div><!--end row middle-->

<!-- MODALS -->

<!-- Half Left Modal: Panel type 2 -->
<div class="modal fade bd-example-modal-lg" id="leftFiftyModal" tabindex="-1" role="dialog" aria-labelledby="leftFiftyLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
          
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
                  <?php include 'widgets.php'; ?>
            <hr>
            <h3>Create your own content</h3>
            <form id="leftHalfForm" method="post">
              <input type="hidden" name="page_content" id="leftHalfPageContent" value="">
              <input type="hidden" name="panel_type" value="2">
              <input type="hidden" id="page_id" name="page_id">
            <textarea class="original" id="leftHalfTextArea" name="halfLeftText"></textarea>
            <input type="submit" value="Save Content">
          </form>
        </div>
        <div class="modal-footer">
        </div>
     </div>
  </div>
</div>

<!-- Half Right Modal: Panel Type 3 -->
<div class="modal fade  bd-example-modal-lg" id="rightFiftyModal" tabindex="-1" role="dialog" aria-labelledby="rightFiftyLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
         
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h3>Images</h3>
                <?php include 'widgets.php'; ?>
          <hr>
          <h3>Create your own content</h3>
          <form id="rightHalfForm" method="post">
          <input type="hidden" name="page_content" id="rightHalfPageContent" value="">
            <input type="hidden" name="panel_type" value="3">
            <input type="hidden"  name="page_id">
          <textarea  class="original" id="rightHalfTextArea" name="halfRightText"></textarea>
          <input type="submit" value="Save Content">
        </form>
      </div>
        <div class="modal-footer">
        </div>
     </div>
  </div>
</div>

<!-- Top Left Modal: Panel Type 4 -->
<div class="modal fade  bd-example-modal-lg" id="topLeftModal" tabindex="-1" role="dialog" aria-labelledby="topLeftLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
      
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h3>Images</h3>
                <?php include 'widgets.php'; ?>
          <hr>
          <h3>Create your own content</h3>
          <form id="topLeftForm" method="post">
          <input type="hidden" name="page_content" id="topLeftPageContent" value="">
            <input type="hidden" name="panel_type" value="4">
            <input type="hidden"  name="page_id">
          <textarea  class="original" id="topLeftTextArea" name="topleftText"></textarea>
          <input type="submit" value="Save Content">
        </form>
      </div>
        <div class="modal-footer">
        </div>
     </div>
  </div>
</div>


<!-- Top Right Modal: Panel Type 5  -->
<div class="modal fade  bd-example-modal-lg" id="topRightModal" tabindex="-1" role="dialog" aria-labelledby="topRightLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
       
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h3>Images</h3>
                <?php include 'widgets.php'; ?>
          <hr>
          <h3>Create your own content</h3>
          <form id="topRightForm" method="post">
          <input type="hidden" name="page_content" id="topRightPageContent" value="">
            <input type="hidden" name="panel_type" value="5">
            <input type="hidden"  name="page_id">
          <textarea  class="original" id="topRightTextArea" name="topRightText"></textarea>
          <input type="submit" value="Save Content">
        </form>
      </div>
        <div class="modal-footer">
        </div>
     </div>
  </div>
</div>


<!-- Bottom Left Modal:Panel Type 6  -->
<div class="modal fade  bd-example-modal-lg" id="bottomLeftModal" tabindex="-1" role="dialog" aria-labelledby="bottomLeftLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
         
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h3>Images</h3>
                <?php include 'widgets.php'; ?>
          <hr>
          <h3>Create your own content</h3>
          <form id="bottomLeftForm" method="post">
          <input type="hidden" name="page_content" id="bottomLeftPageContent" value="">
            <input type="hidden" name="panel_type" value="6">
            <input type="hidden"  name="page_id">
          <textarea  class="original" id="bottomLeftTextArea" name="bottomLeftText"></textarea>
          <input type="submit" value="Save Content">
        </form>
      </div>
        <div class="modal-footer">
        </div>
     </div>
  </div>
</div>



<!-- Bottom Right Modal: Panel Type 7 -->
<div class="modal fade  bd-example-modal-lg" id="bottomRightModal" tabindex="-1" role="dialog" aria-labelledby="bottomRightLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h3>Images</h3>
                <?php include 'widgets.php'; ?>
          <hr>
          <h3>Create your own content</h3>
          <form id="bottomRightForm" method="post">
          <input type="hidden" name="page_content" id="bottomRightPageContent" value="">
            <input type="hidden" name="panel_type" value="7">
            <input type="hidden"  name="page_id">
          <textarea  class="original" id="bottomRightTextArea" name="bottomRightText"></textarea>
          <input type="submit" value="Save Content">
        </form>
      </div>
        <div class="modal-footer">
        </div>
     </div>
  </div>
</div>

<div class="row bottom">
<div class="col-lg-12">
  <div class="marquee"><h2>Ticker</h2></div>
</div>
</div>