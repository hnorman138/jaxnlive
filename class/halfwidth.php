<?php include '../getpages.php'?>


<div class="row top">
	<?php include 'banner.php'?>
</div>

<div style="text-align: center; margin-top: 15px;">
	<p>(Content must fit within bounds of dotted border)</p>
</div>

<div class="row middle">
	<div class="col-lg-6 leftFifty" id="leftFifty">
	  	<div class="leftContent" id="leftContent" style="background-color: white; border: dotted 1px black;">		
	  		
	  	</div>
	</div>
	<div class="col-lg-6 rightFifty" id="rightFifty">
	  <div class="rightContent" id="rightContent" style="background-color: white; border: dotted 1px black; ">
	  	
	  </div>
	</div>
</div>

<div class="modal fade bd-example-modal-lg" id="leftFiftyModal" tabindex="-1" role="dialog" aria-labelledby="leftFiftyLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      	<div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	          </button>
	        </div>
	        <div class="modal-body">
	        	<?php include 'widgets.php'; ?>	    	
	        	<hr>
	        	<h3>Create your own content</h3>
	        	<form id="leftHalfForm" method="post">
	        	  <input type="hidden" name="page_content" id="leftHalfPageContent" value="">
	        	  <input type="hidden" name="panel_type" value="2">
	        	  <input type="hidden" id="page_id" name="page_id">
			      <textarea class="original" id="leftHalfTextArea" name="halfLeftText"></textarea>
			      <input type="submit" value="Save Content">
			    </form>
		    </div>
		    <div class="modal-footer">
		    </div>
	   </div>
	</div>
</div>

<div class="modal fade  bd-example-modal-lg" id="rightFiftyModal" tabindex="-1" role="dialog" aria-labelledby="rightFiftyLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        	
	    	<?php include 'widgets.php'; ?>



        	<hr>
        	<h3>Create your own content</h3>
        	<form id="rightHalfForm" method="post">
	    	  <input type="hidden" name="page_content" id="rightHalfPageContent" value="">
        	  <input type="hidden" name="panel_type" value="3">
        	  <input type="hidden"  name="page_id">
		      <textarea  class="original" id="rightHalfTextArea" name="halfRightText"></textarea>
		      <input type="submit" value="Save Content">
		    </form>
	    </div>
	    <div class="modal-footer">
	    </div>
	   </div>
	</div>
</div>

<div class="row bottom">
<div class="col-lg-12">
	<div class="marquee"><h2>Ticker</h2></div>
</div>
</div>