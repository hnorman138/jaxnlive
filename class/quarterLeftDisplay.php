<?php include '../getPages.php'?>

<div class="row top">
	<?php include 'banner.php' ?>
</div>

<div class="row middle">
<?php
function cmp($a, $b) {
    $goodOrder = [4,6,3];
    return array_search($a, $goodOrder) - array_search($b, $goodOrder);
}

uksort($data_array, "cmp");
?>

<?php foreach($data_array as $panel_type_id => $content): ?>
<?php if($panel_type_id == 4){ ?>
<!-- Left -->
<div class="col-lg-6"  >
  <div class="row" style="height:50%; padding-bottom: 15px;">
  	<div class="col-lg-12" style="height:100%;">
  		<div class="topLeftContent" id="topLeftContent">
      <?php echo $content?>
      </div>
  	</div>
  </div>
<?php } elseif($panel_type_id == 6){?>
  <div class="row" style="height:50%; padding-top: 15px;">
  	<div class="col-lg-12" style="height:100%;">
  		<div class="bottomLeftContent" id="bottomLeftContent">
        <?php echo $content?>
      </div>
  	</div>
  </div>
</div>
<?php } elseif($panel_type_id == 3){?>
<div class="col-lg-6" >
  	<div class="rightContent" id="rightContent">
      <?php echo $content?>
    </div>
</div>
<?php } ?>
<?php endforeach; ?>
</div><!--end row middle-->

<div class="row bottom">
  <div class="col-lg-12">
    <div><h2>Ticker</h2></div>
  </div>
</div>