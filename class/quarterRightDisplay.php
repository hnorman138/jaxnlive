<?php include '../getPages.php'?>

<div class="row top">
	<?php include 'banner.php' ?>
</div>

<div class="row middle">
<?php
function cmp($a, $b) {
    $goodOrder = [2,5,7];
    return array_search($a, $goodOrder) - array_search($b, $goodOrder);
}

uksort($data_array, "cmp");
?>

<?php foreach($data_array as $panel_type_id => $content): ?>
<?php if($panel_type_id == 2){ ?>
<!-- right -->
<div class="col-lg-6" >
    <div class="leftContent" id="leftContent" >
      <?php echo $content?>
    </div>
</div>
<?php } elseif($panel_type_id == 5){?>
<div class="col-lg-6"  >
  <div class="row" style="height:50%; padding-bottom: 15px;">
    <div class="col-lg-12" style="height:100%; ">
      <div class="topRightContent" id="topRightContent" >
        <?php echo $content?>
      </div>
    </div>
  </div>
  <?php } elseif($panel_type_id == 7){?>
  <div class="row" style="height:50%; padding-top: 15px;">
    <div class="col-lg-12" style="height:100%;">
      <div class="bottomRightContent" id="bottomRightContent">
        <?php echo $content?>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php endforeach ?>
</div><!--end row middle-->

<div class="row bottom">
  <div class="col-lg-12">
    <div><h2>Ticker</h2></div>
  </div>
</div>