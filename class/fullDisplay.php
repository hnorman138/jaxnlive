<?php include '../getPages.php'?>

<div class="row top">
	<?php include 'banner.php'?>
</div>

<div class="row middle">
	<?php foreach($panelResult as $PR): ?>
		<?php if($PR['panel_type_id'] ==1){?>
	<div class="col-lg-12">
		<div class="fullContent" id="fullContent">
	  	<?php echo $PR['content']?>
	  	</div>
	</div>
	<?php } ?>
	<?php endforeach; ?>
</div>

<div class="row bottom">
	<div class="col-lg-12">
		<div><h2>Ticker</h2></div>
	</div>
</div>