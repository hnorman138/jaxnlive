<?php include '../getPages.php'?>

<div class="row top">
	<?php include 'banner.php' ?>
</div>


<div class="row middle">
	<?php foreach($panelResult as $PR): ?>
	<?php if($PR['panel_type_id'] == 2){ ?>
	<div class="col-lg-6 leftFifty">
		
	  	<div class="leftContent" id="leftContent" >		
		<?php echo $PR['content']?>
	  	</div>
	</div>
	<?php } elseif($PR['panel_type_id'] == 3){?>
	<div class="col-lg-6 rightFifty">
		
	  <div class="rightContent" id="rightContent" >
	  	<?php  echo $PR['content'] ?>
	  </div>

	</div>
	<?php } ?>
	<?php endforeach; ?>
</div>

<div class="row bottom">
	<div class="col-lg-12">
		<div><h2>Ticker</h2></div>
	</div>
</div>