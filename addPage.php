<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'dbConnect.php';



$title = $_POST['addTitle'];
$page_type = $_POST['pageType'];
$display_id = $_POST['displayId'];
$start_time = $_POST['datePickerStart'];
$end_time = $_POST['datePicker'];
$duration = $_POST['durationSet'];

$newEndTime = DateTime::createFromFormat('m/d/Y h:i A', $end_time);
$convertedDateTime = $newEndTime->format('Y-m-d H:i:s');

$newStartTime = DateTime::createFromFormat('m/d/Y h:i A', $start_time);
$convertedStartDate = $newStartTime->format('Y-m-d H:i:s');

$addpage = "
	INSERT INTO jaxnlive.pages (title, page_type_id, display_id, start_time, end_time, duration)
	VALUES ('$title','$page_type', '$display_id','$convertedStartDate', '$convertedDateTime', '$duration');
";

if ($mysqlConn->query($addpage) === TRUE) {
	$last_id = $mysqlConn->insert_id;
	$data['last_insert_id'] = $last_id;
	echo json_encode($data);

} else {
    echo "Error: " . $addpage . "<br>" . $mysqlConn->error;
}

$addPageToDisplay = "
	INSERT INTO jaxnlive.display_to_page(page_id, display_id)
	VALUES ('$last_id', '$display_id');
";

if ($mysqlConn->query($addPageToDisplay) === TRUE) {

} else {
    echo "Error: " . $addPageToDisplay . "<br>" . $mysqlConn->error;
}

mysqli_close($mysqlConn);
?>