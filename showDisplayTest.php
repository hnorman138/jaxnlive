<?php include 'getPages.php';?>


<!DOCTYPE html>
<html>
<head>
    <title>Template One</title>

    <style type="text/css">
        html,
        body {
          height: 100vh;
          width: 100vw;
          overflow: hidden;
        }

        .my-container {
          display: flex;
          flex-direction: column;
          height: 100vh;
          width:100vw;
        }

        .my-container>.top [class^="col-"],
        .my-container>.bottom [class^="col-"] {
          background-color: #778899  ;
          color: white;
          text-align: center;
        }

        .my-container>.middle {
          flex-grow: 1;
          padding:30px;
          /*background-image: url('images/bg_green.svg');*/
          background-size: cover;
        }

        .my-container>.middle>* {
        }

        #clock{
            /*background-color:#333;*/
            font-family: sans-serif;
            font-size:40px;
            text-shadow:0px 0px 1px #fff;
            color:#fff;
        }
        #clock span {
            color:#fff;
            font-size:40px;
            position:relative;

        }
        #date {
            margin-top: -10px;
            letter-spacing:3px;
            font-size:20px;
            font-family:arial,sans-serif;
            color:#fff;
        }


    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
         
    <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
    <script type='text/javascript' src='//cdn.jsdelivr.net/jquery.marquee/1.4.0/jquery.marquee.min.js'></script>

</head>
<body onload="startTime()">
<div class="container-fluid my-container">

    <div class="row top">
        <?php include 'banner.php'?>
    </div>

    <div class="row middle" id="middle" style="background-image: url();">
        <!-- Full Page Divs -->
        <div class="col-lg-12" id="fullColumn">
            <div class="fullContent" id="fullContent" style="height: 100%; ">
            </div>
        </div>
        <!-- End Full Page Divs -->

        <!-- Half Page Divs -->
        <div class="col-lg-6 leftColumn" id="leftColumn">
            
            <div class="leftContent" id="leftContent" style=" height: 100%; ">        

            </div>
        </div>

        <div class="col-lg-6 rightColumn" id="rightColumn">
            
          <div class="rightContent" id="rightContent" style=" height: 100%; ">

          </div>

        </div>
        <!-- End Half Page Divs -->

        <!-- Quarter Page Divs -->

        <!-- Left -->
        <div class="col-lg-6"  id="leftColumnQtr">
          <div class="row" style="height:50%; padding-bottom: 15px;">
            <div class="col-lg-12" style="height:100%;">
                <div  id="topLeftContent" style=" height: 100%; ">
              </div>
            </div>
          </div>
          <div class="row" style="height:50%; padding-top: 15px;">
            <div class="col-lg-12" style="height:100%;">
                <div id="bottomLeftContent" style=" height: 100%;">
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6" id="rightColumnQtrHalf" >
            <div id="rightQtrContent" style=" height: 100%; ">
            </div>
        </div>

        <!-- right -->
        <div class="col-lg-6" id="rightColumnQtr">
            <div id="leftQtrContent" style="height: 100%;">
            </div>
        </div>
        <div class="col-lg-6" id="leftColumnQtrHalf" >
          <div class="row" style="height:50%; padding-bottom: 15px;">
            <div class="col-lg-12" style="height:100%;">
              <div id="topRightContent" style=" height: 100%;">
              </div>
            </div>
          </div>
          <div class="row" style="height:50%; padding-top: 15px;">
            <div class="col-lg-12" style="height:100%;">
              <div id="bottomRightContent" style=" height: 100%;">
              </div>
            </div>
          </div>
        </div>
        <!-- End Quarter Page Divs -->
    </div>
    <!-- End Row Middle -->

    <div class="row bottom">
        <?php include 'ticker.php';?>
    </div>
</div>

<script type="text/javascript">
    let obj = <?php echo $showDisplays; ?>;

    let counter = 0;

    var fullContent = document.getElementById('fullContent');
    var leftContent = document.getElementById('leftContent');
    var rightContent = document.getElementById('rightContent');
    var topLeftContent = document.getElementById('topLeftContent');
    var topRightContent = document.getElementById('topRightContent');
    var bottomLeftContent = document.getElementById('bottomLeftContent');
    var bottomRightContent = document.getElementById('bottomRightContent');
    var leftQtrContent = document.getElementById('leftQtrContent');
    var rightQtrContent = document.getElementById('rightQtrContent');

    var fullColumn = document.getElementById('fullColumn');
    var leftColumn = document.getElementById('leftColumn');
    var rightColumn = document.getElementById('rightColumn');
    var leftColumnQtr = document.getElementById('leftColumnQtr');
    var rightColumnQtrHalf = document.getElementById('rightColumnQtrHalf');
    var rightColumnQtr = document.getElementById('rightColumnQtr');
    var leftColumnQtrHalf = document.getElementById('leftColumnQtrHalf');

    const pages_array = obj.reduce(function(pages_array, item, index, obj) {
      const current_pageID = item.pageID;
      const current_pageType = item.page_type_id;
      const duration = item.duration;
      const exisiting_page = pages_array.find(page => page.pageID === current_pageID);

      if (exisiting_page === undefined) {
        const new_Page = {
          pageID: current_pageID,
          pageType: current_pageType,
          duration: duration,
          content: [item]
        }
        pages_array.push(new_Page);
      } else {
        exisiting_page.content.push(item)
      }

      return pages_array;
    }, []);
    
    reloadFunction();
    setInterval(reloadFunction, parseInt(pages_array[counter].duration)*2000);

    function reloadFunction(){
    const currentJSONobject = pages_array[counter]; 
    $('#middle').css('background-image', 'url(' + currentJSONobject.content[0].background_img + ')');

    fullContent.innerHTML = '';
    rightContent.innerHTML = '';
    leftContent.innerHTML = '';
    topLeftContent.innerHTML = '';
    topRightContent.innerHTML = '';
    bottomLeftContent.innerHTML = '';
    bottomRightContent.innerHTML = '';

   for(var i = 0; i < currentJSONobject.content.length; i++){

        if(parseInt(pages_array[counter].pageType) == 1){
            console.log("Paren pageType => ", pages_array[counter].pageType);

            fullContent.innerHTML = currentJSONobject.content[i].panel_type_id == 1 ? currentJSONobject.content[i].content : fullContent.innerHTML;

            
            fullColumn.style.display = "block";


            leftColumn.style.display = "none";
            rightColumn.style.display = "none";
            leftColumnQtr.style.display = "none";
            rightColumnQtrHalf.style.display = "none";
            rightColumnQtr.style.display = "none";
            leftColumnQtrHalf.style.display = "none";

        } if(parseInt(pages_array[counter].pageType) == 2){
            console.log("Paren pageType => ", pages_array[counter].pageType);

            leftContent.innerHTML = currentJSONobject.content[i].panel_type_id == 2 ? currentJSONobject.content[i].content : leftContent.innerHTML;
            rightContent.innerHTML = currentJSONobject.content[i].panel_type_id == 3 ? currentJSONobject.content[i].content : rightContent.innerHTML;

            leftColumn.style.display = "block";
            rightColumn.style.display = "block";

            fullColumn.style.display = "none";
            leftColumnQtr.style.display = "none";
            rightColumnQtrHalf.style.display = "none";
            rightColumnQtr.style.display = "none";
            leftColumnQtrHalf.style.display = "none";


        } if(parseInt(pages_array[counter].pageType) == 3){
            console.log("Paren pageType => ", pages_array[counter].pageType);

            rightQtrContent.innerHTML = currentJSONobject.content[i].panel_type_id == 3 ? currentJSONobject.content[i].content : rightQtrContent.innerHTML;
            topLeftContent.innerHTML = currentJSONobject.content[i].panel_type_id == 4 ? currentJSONobject.content[i].content : topLeftContent.innerHTML;
            bottomLeftContent.innerHTML = currentJSONobject.content[i].panel_type_id == 6 ? currentJSONobject.content[i].content : bottomLeftContent.innerHTML;

            leftColumnQtr.style.display = "block";
            rightColumnQtrHalf.style.display = "block";

            fullColumn.style.display = "none";
            leftColumn.style.display = "none";
            rightColumn.style.display = "none";
            rightColumnQtr.style.display = "none";
            leftColumnQtrHalf.style.display = "none";


        } if(parseInt(pages_array[counter].pageType) == 4){
            console.log("Paren pageType => ", pages_array[counter].pageType);

            leftQtrContent.innerHTML = currentJSONobject.content[i].panel_type_id == 2 ? currentJSONobject.content[i].content : leftQtrContent.innerHTML;
            topRightContent.innerHTML = currentJSONobject.content[i].panel_type_id == 5 ? currentJSONobject.content[i].content : topRightContent.innerHTML;
            bottomRightContent.innerHTML = currentJSONobject.content[i].panel_type_id == 7 ? currentJSONobject.content[i].content : bottomRightContent.innerHTML;

            rightColumnQtr.style.display = "block";
            leftColumnQtrHalf.style.display = "block";

            fullColumn.style.display = "none";
            leftColumn.style.display = "none";
            rightColumn.style.display = "none";
            leftColumnQtr.style.display = "none";
            rightColumnQtrHalf.style.display = "none";

        }

    }

    console.log(pages_array[counter])

    counter += 1; 
    if (counter === pages_array.length){
        counter = 0; 
    }

} 
console.log(obj);
console.log(pages_array);
</script>


<!-- JS -->
<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>

 <script src="js/clock.js"></script>
 <script type="js/cycle.js"></script>

 <script type="text/javascript">
    $('.marquee').marquee({
        duration: 30000,
        gap:30,
    });
 </script>

</body>
</html>

