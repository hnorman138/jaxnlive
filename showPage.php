<?php include 'getPages.php'; ?>

<!DOCTYPE html>
<html>
<head>
<title>Template One</title>

<style type="text/css">
@import "style.css";

html,
body {
height: 100vh;
width: 100vw;
overflow: hidden;
}

iframe{
height:100% !important;
width:100% !important;
}

.middle p{
max-height:100%;
margin-bottom:0;     
position: absolute; 
top:0; 
right:0; 
bottom:0; 
left:0;
display:flex;
}

img {
object-fit: scale-down;
width: 100%; 
margin:0 auto;
}

#fullContent{
display:flex;
justify-content:center;
align-items:center;
overflow: hidden;
}

#fullContent img{
object-fit: contain;
height:100%;
}

#facebook {
  opacity: 1 !important;
}

.fullContent {
max-width: 100%;
max-height: 100%;
height: auto;
width: auto;
}

.fullContent > img{
min-width:100%;
min-height: 100%;
width: auto;
height: auto;
}

/*QUARTER  and HALF CONTENT EDITOR*/
#leftContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#leftContent img{
    object-fit: contain;
}
.leftContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.leftContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#topRightContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#topRightContent img{
    object-fit: contain;
}
.topRightContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.topRightContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#bottomRightContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#bottomRightContent img{
    object-fit: contain;
}
.bottomRightContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.bottomRightContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#topLeftContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#topLeftContent img{
    object-fit: contain;
}
.topLeftContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.topLeftContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#bottomLeftContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#bottomLeftContent img{
    object-fit: contain;
}
.bottomLeftContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.bottomLeftContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#rightContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#rightContent img{
    object-fit: contain;
}
.rightContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.rightContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
/*END EDITOR RESPONSIVE STYLES*/



.my-container {
display: flex;
flex-direction: column;
justify-content: center;
height: 100vh;
width:100vw;
}

.my-container .top.row, 
.my-container .row.bottom {
flex-shrink: 0;
}

.my-container>.top [class^="col-"],
.my-container>.bottom [class^="col-"] {
background-color: #778899  ;
color: white;
text-align: center;
}

.my-container>.middle {
flex-grow: 1;
padding:30px;
/*background-image: url('images/bg_green.svg');*/
background-size: cover;
}

.my-container>.middle>* {
}

#clock{
    /*background-color:#333;*/
    font-family: sans-serif;
    font-size:40px;
    text-shadow:0px 0px 1px #fff;
    color:#fff;
}
#clock span {
    color:#fff;
    font-size:40px;
    position:relative;

}
#date {
    margin-top: -10px;
    letter-spacing:3px;
    font-size:20px;
    font-family:arial,sans-serif;
    color:#fff;
}
	
</style>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <link href="https://rawgit.com/tempusdominus/bootstrap-4/master/build/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="src/ytv.css" type="text/css" rel="stylesheet" />

  <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="instafeed.js-master/instafeed.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.0/moment-with-locales.js"></script>
  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="https://rawgit.com/tempusdominus/bootstrap-4/master/build/js/tempusdominus-bootstrap-4.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
  <script src="src/ytv.js" type="text/javascript"></script>


</head>
<body onload="startTime()">
<div class="container-fluid my-container">
	<?php foreach($pageResult as $page): ?>
	<?php if($page['page_type_id'] == 1){ ?>
		
		<!-- full page -->
		<?php include 'class/fullDisplay.php'?>

	<?php } elseif($page['page_type_id'] == 2){ ?>
		
		<!-- Half -->
		<?php include 'class/halfDisplay.php'?>

	<?php } elseif($page['page_type_id'] == 3){ ?>
		
		<!-- QuarterLeft -->
		<?php include 'class/quarterLeftDisplay.php'?>

	<?php } elseif($page['page_type_id'] == 4){ ?>
		
		<!-- QuarterRight -->
		<?php include 'class/quarterRightDisplay.php'?>

	<?php }?>
	<?php endforeach?>

</div>

<script type="text/javascript">
  var bg = "<?php echo $page['background_img'] ?>";
  console.log(bg);
  $('.middle').css('background-image', 'url(' + bg + ')');
</script>




<!-- JS -->
<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>

 <script src="js/clock.js"></script>

 <script type="text/javascript">
 	$('.marquee').marquee({
 		duration: 30000,
 		gap:30,
 	});
 </script>


<script>
     jQuery(document).ready(function(){

    $(window).scroll(function() {
   if($(window).scrollTop() + $(window).height() == $(document).height()) {
          // call feed.next() once the page reaches the bottom
         feed.next();

     }
});

      });
</script>

<script type="text/javascript">
$(document).ready(function() {
  var feed = new Instafeed({
    get: 'user',
    userId: '5590008752',
    clientId: 'e228035bad48471a94244cc6305cf827',
    accessToken: '5590008752.e228035.07bcba1d14024e8c82081c50e59023e5',
    // template: '<a class="gallery-item" target="_blank" href="{{link}}"><img class="image_img" src="{{image}}" /><div class="caption_layer"><p class="instaCaption">{{model.likes.count}} Likes</p></div></a>',
    template: '<a target="_blank" href="{{link}}"><img src="{{image}}" width="200" height="200"/><div class="caption">{{caption}}</div></a>',
    sortBy: 'most-recent',
    limit: '4',
    // template: '<div class="wrap"><img src="{{image}}"/><div class="caption">{{caption}}</div></div>',
    useHttp: true
    });

  feed.run();

  // RegEx Source:  https://stackoverflow.com/a/37704433/362536
  const youtubeUrlPattern = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;

  [...document.querySelectorAll('iframe')].filter((iframeEl) => {
    // Filter to iframes loading YouTube URLs only.
    return iframeEl.src.match(youtubeUrlPattern);
  }).forEach((iframeEl) => {
    const a = document.createElement('a');
    a.href = iframeEl.src;
    a.search = a.search || '?';
    a.search += '&autoplay=1&loop=1&rel=0';
    iframeEl.src = a.href;
  });
});
</script>

<script type="text/javascript">
  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=2691675043";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

</body>
</html>