JAXN Live
=========

Purpose
-------
This is the set of code that implements JAXN Live - a CMS builetin board of sorts that is displayed on various TV displays throughout the main plant which are each (currently) connected to RasBerry Pi's running Chrome and each displaying a specific URL.

As of this writing, Amy Flamenco is the end user in charge of managing the content which gets displayed on each screen

Hosting
-------
Currnetly this is hosted on the main production web server at 192.168.20.11 running Windows 2008 R2 Standard. It resides in the directory `C:\Websites\Jaxnlive\jaxnlive` and uses the `jaxnlive` IIS instance

Deployment
----------
To deploy code changes, first clone the Git repo on Bitbucket. Commit or merge changes then `git push` them back up, log into the server, open git bash, change to the residing directory, and run `git pull`.