<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'dbConnect.php';

$statusMsg = '';

// File upload path
$targetDir = getcwd() . "/images/";
$fileName = basename($_FILES["fileBG"]["name"]);
$targetFilePath = $targetDir . $fileName;
$imgPath = 'images/'.$fileName;
$imgType = 'background';
$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);


if(isset($_POST["submitBG"]) && !empty($_FILES["fileBG"]["name"])){
    // Allow certain file formats
    $allowTypes = array('jpg', 'JPG','png', 'PNG', 'JPEG', 'jpeg', 'GIF', 'gif','PDF','pdf','mp4','MP4');
    if(in_array($fileType, $allowTypes)){
        // Upload file to server
        if(move_uploaded_file($_FILES["fileBG"]["tmp_name"], $targetFilePath)){
            // Insert image file name into database
            $insert = $mysqlConn->query("INSERT into jaxnlive.images (image_name, url, type) VALUES ('".$fileName."', '".$imgPath."', '".$imgType."')");
            if($insert){
                $statusMsg = "The file ".$fileName. " has been uploaded successfully.";
                header("Location: contentLibrary.php");
            }else{
                $statusMsg = "File upload failed, please try again.";
            } 
        }else{
            $statusMsg = "Sorry, there was an error uploading your file.";
        }
    }else{
        $statusMsg = 'Sorry, only JPG, JPEG, PNG, GIF,MP4, & PDF files are allowed to upload.';
    }
}else{
    $statusMsg = 'Please select a file to upload.';
}

// Display status message
echo $statusMsg;


?>
