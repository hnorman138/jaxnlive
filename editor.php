<!DOCTYPE html>
<html>
<head>
  <script src="tinyMCE/js/tinymce/tinymce.min.js"></script>
  <script type="text/javascript">
  tinymce.init({
    selector: '#mytextarea',
	height: 500,
  theme: 'modern',
  plugins: 'print preview fullpage paste searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools media  link contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
  });
  </script>
</head>
<body>
    <?php include("menu.php");?>
    <?php startblock('article') ?>

      	<div>
      		<h1 style="text-align: center; color: #A9BD50; ">Content Management Editor</h1>
      	</div>
        <form method="post">
          <textarea id="mytextarea"></textarea>
        </form>
        <div>
        	<h1 style="text-align: center; color: #A9BD50; ">Preview</h1>
        </div>


    <?php endblock() ?>
</body>
</html>