<?php include 'getPages.php';?>


<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="refresh" content="500"/>
    <title>Template One</title>

<style type="text/css">
html,
body {
height: 100vh;
width: 100vw;
overflow: hidden;
}

iframe{
height:100% !important;
width:100% !important;
}

.middle p{
max-height:100%;
margin-bottom:0;     
position: absolute; 
top:0; 
right:0; 
bottom:0; 
left:0;
display:flex;
}

img {
object-fit: scale-down;
width: 100%; 
margin:0 auto;
}

#fullContent{
display:flex;
justify-content:center;
align-items:center;
overflow: hidden;
}

#fullContent img{
object-fit: contain;
height:100%;
}

.fullContent {
max-width: 100%;
max-height: 100%;
height: auto;
width: auto;
}

.fullContent > img{
min-width:100%;
min-height: 100%;
width: auto;
height: auto;
}

/*QUARTER  and HALF CONTENT EDITOR*/
#leftContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#leftContent img{
    object-fit: contain;
}
.leftContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.leftContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#topRightContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#topRightContent img{
    object-fit: contain;
}
.topRightContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.topRightContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#bottomRightContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#bottomRightContent img{
    object-fit: contain;
}
.bottomRightContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.bottomRightContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#topLeftContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#topLeftContent img{
    object-fit: contain;
}
.topLeftContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.topLeftContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#bottomLeftContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#bottomLeftContent img{
    object-fit: contain;
}
.bottomLeftContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.bottomLeftContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#rightContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#rightContent img{
    object-fit: contain;
}
.rightContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.rightContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
/*END EDITOR RESPONSIVE STYLES*/



.my-container {
display: flex;
flex-direction: column;
justify-content: center;
height: 100vh;
width:100vw;
}

.my-container .top.row, 
.my-container .row.bottom {
flex-shrink: 0;
}

.my-container>.top [class^="col-"],
.my-container>.bottom [class^="col-"] {
background-color: #778899  ;
color: white;
text-align: center;
}

.my-container>.middle {
flex-grow: 1;
padding:30px;
/*background-image: url('images/bg_green.svg');*/
background-size: cover;
}

.my-container>.middle>* {
}

#clock{
    /*background-color:#333;*/
    font-family: sans-serif;
    font-size:40px;
    text-shadow:0px 0px 1px #fff;
    color:#fff;
}
#clock span {
    color:#fff;
    font-size:40px;
    position:relative;

}
#date {
    margin-top: -10px;
    letter-spacing:3px;
    font-size:20px;
    font-family:arial,sans-serif;
    color:#fff;
}

</style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
         
    <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
      <script type="text/javascript" src="instafeed.js-master/instafeed.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
    <script type='text/javascript' src='//cdn.jsdelivr.net/jquery.marquee/1.4.0/jquery.marquee.min.js'></script>

</head>
<body onload="startTime()">
<div class="container-fluid my-container d-flex h-100">
<div class="row top">
    <?php include 'banner.php'?>
</div>

    <div class="row middle" id="middle" style="background-image: url();">
        <!-- Full Page Divs -->
        <div class="col-lg-12" id="fullColumn">
            <div class="fullContent" id="fullContent">
            </div>
        </div>
        <!-- End Full Page Divs -->

        <!-- Half Page Divs -->
        <div class="col-lg-6 leftColumn " id="leftColumn" style="align-items: center;">  
            <div class="leftContent" id="leftContent" style=" margin:auto; ">        
            </div>
        </div>
        <div class="col-lg-6 rightColumn" id="rightColumn"> 
          <div class="rightContent" id="rightContent" >
          </div>
        </div>
        <!-- End Half Page Divs -->

        <!-- Quarter Page Divs -->

        <!-- Left -->
        <div class="col-lg-6"  id="leftColumnQtr">
          <div class="row" style="height:50%; padding-bottom: 15px;">
            <div class="col-lg-12" style="height:100%;">
                <div  id="topLeftContent" class="topLeftContent" >
              </div>
            </div>
          </div>
          <div class="row" style="height:50%; padding-top: 15px;">
            <div class="col-lg-12" style="height:100%;">
                <div id="bottomLeftContent" class="bottomLeftContent" >
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6" id="rightColumnQtrHalf" >
            <div id="rightQtrContent" class="rightQtrContent" >
            </div>
        </div>
        <!-- right -->
        <div class="col-lg-6" id="rightColumnQtr">
            <div id="leftQtrContent" class="leftQtrContent" >
            </div>
        </div>
        <div class="col-lg-6" id="leftColumnQtrHalf" >
          <div class="row" style="height:50%; padding-bottom: 15px;">
            <div class="col-lg-12" style="height:100%;">
              <div id="topRightContent" class="topRightContent" >
              </div>
            </div>
          </div>
          <div class="row" style="height:50%; padding-top: 15px;">
            <div class="col-lg-12" style="height:100%;">
              <div id="bottomRightContent" class="bottomRightContent">
              </div>
            </div>
          </div>
        </div>
        <!-- End Quarter Page Divs -->
    </div>
    <!-- End Row Middle -->

    <div class="row bottom">
        <?php include 'ticker.php';?>
    </div>
</div>



<!-- JS -->
<script type="text/javascript">
let obj = <?php echo $showDisplays; ?>;


let counter = 0;

var fullContent = document.getElementById('fullContent');
var leftContent = document.getElementById('leftContent');
var rightContent = document.getElementById('rightContent');
var topLeftContent = document.getElementById('topLeftContent');
var topRightContent = document.getElementById('topRightContent');
var bottomLeftContent = document.getElementById('bottomLeftContent');
var bottomRightContent = document.getElementById('bottomRightContent');
var leftQtrContent = document.getElementById('leftQtrContent');
var rightQtrContent = document.getElementById('rightQtrContent');

var fullColumn = document.getElementById('fullColumn');
var leftColumn = document.getElementById('leftColumn');
var rightColumn = document.getElementById('rightColumn');
var leftColumnQtr = document.getElementById('leftColumnQtr');
var rightColumnQtrHalf = document.getElementById('rightColumnQtrHalf');
var rightColumnQtr = document.getElementById('rightColumnQtr');
var leftColumnQtrHalf = document.getElementById('leftColumnQtrHalf');

const pages_array = obj.reduce(function(pages_array, item, index, obj) {
  const current_pageID = item.pageID;
  const current_pageType = item.page_type_id;
  const duration = item.duration;
  const exisiting_page = pages_array.find(page => page.pageID === current_pageID);

  if (exisiting_page === undefined) {
    const new_Page = {
      pageID: current_pageID,
      pageType: current_pageType,
      duration: duration,
      content: [item]
    }
    pages_array.push(new_Page);
  } else {
    exisiting_page.content.push(item)
  }

  return pages_array;
}, []);

reloadFunction();

function reloadFunction(){
const currentJSONobject = pages_array[counter]; 

$('#middle').css('background-image', 'url(' + currentJSONobject.content[0].background_img + ')');

fullContent.innerHTML = '';
rightContent.innerHTML = '';
leftContent.innerHTML = '';
topLeftContent.innerHTML = '';
topRightContent.innerHTML = '';
bottomLeftContent.innerHTML = '';
bottomRightContent.innerHTML = '';

for(var i = 0; i < currentJSONobject.content.length; i++){

    if(parseInt(pages_array[counter].pageType) == 1){
        console.log("Paren pageType => ", pages_array[counter].pageType);


        const youtubeUrlPattern = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;

        [...document.querySelectorAll('iframe')].filter((iframeEl) => {
          // Filter to iframes loading YouTube URLs only.
          return iframeEl.src.match(youtubeUrlPattern);
        }).forEach((iframeEl) => {
          const a = document.createElement('a');
          a.href = iframeEl.src;
          a.search = a.search || '?';
          a.search += '&autoplay=1&mute=1';
          iframeEl.src = a.href;
        });

        fullContent.innerHTML = currentJSONobject.content[i].panel_type_id == 1 ? currentJSONobject.content[i].content : fullContent.innerHTML;


        fullColumn.style.display = "block";


        leftColumn.style.display = "none";
        rightColumn.style.display = "none";
        leftColumnQtr.style.display = "none";
        rightColumnQtrHalf.style.display = "none";
        rightColumnQtr.style.display = "none";
        leftColumnQtrHalf.style.display = "none";

    } if(parseInt(pages_array[counter].pageType) == 2){
        console.log("Paren pageType => ", pages_array[counter].pageType);

          const youtubeUrlPattern = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;

        [...document.querySelectorAll('iframe')].filter((iframeEl) => {
          // Filter to iframes loading YouTube URLs only.
          return iframeEl.src.match(youtubeUrlPattern);
        }).forEach((iframeEl) => {
          const a = document.createElement('a');
          a.href = iframeEl.src;
          a.search = a.search || '?';
          a.search += '&autoplay=1&mute=1';
          iframeEl.src = a.href;
        });

        leftContent.innerHTML = currentJSONobject.content[i].panel_type_id == 2 ? currentJSONobject.content[i].content : leftContent.innerHTML;
        rightContent.innerHTML = currentJSONobject.content[i].panel_type_id == 3 ? currentJSONobject.content[i].content : rightContent.innerHTML;

        leftColumn.style.display = "block";
        rightColumn.style.display = "block";

        fullColumn.style.display = "none";
        leftColumnQtr.style.display = "none";
        rightColumnQtrHalf.style.display = "none";
        rightColumnQtr.style.display = "none";
        leftColumnQtrHalf.style.display = "none";


    } if(parseInt(pages_array[counter].pageType) == 3){
        console.log("Paren pageType => ", pages_array[counter].pageType);

          const youtubeUrlPattern = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;

        [...document.querySelectorAll('iframe')].filter((iframeEl) => {
          // Filter to iframes loading YouTube URLs only.
          return iframeEl.src.match(youtubeUrlPattern);
        }).forEach((iframeEl) => {
          const a = document.createElement('a');
          a.href = iframeEl.src;
          a.search = a.search || '?';
          a.search += '&autoplay=1&mute=1';
          iframeEl.src = a.href;
        });

        rightQtrContent.innerHTML = currentJSONobject.content[i].panel_type_id == 3 ? currentJSONobject.content[i].content : rightQtrContent.innerHTML;
        topLeftContent.innerHTML = currentJSONobject.content[i].panel_type_id == 4 ? currentJSONobject.content[i].content : topLeftContent.innerHTML;
        bottomLeftContent.innerHTML = currentJSONobject.content[i].panel_type_id == 6 ? currentJSONobject.content[i].content : bottomLeftContent.innerHTML;

        leftColumnQtr.style.display = "block";
        rightColumnQtrHalf.style.display = "block";

        fullColumn.style.display = "none";
        leftColumn.style.display = "none";
        rightColumn.style.display = "none";
        rightColumnQtr.style.display = "none";
        leftColumnQtrHalf.style.display = "none";


    } if(parseInt(pages_array[counter].pageType) == 4){
        console.log("Paren pageType => ", pages_array[counter].pageType);

          const youtubeUrlPattern = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;

        [...document.querySelectorAll('iframe')].filter((iframeEl) => {
          // Filter to iframes loading YouTube URLs only.
          return iframeEl.src.match(youtubeUrlPattern);
        }).forEach((iframeEl) => {
          const a = document.createElement('a');
          a.href = iframeEl.src;
          a.search = a.search || '?';
          a.search += '&autoplay=1&mute=1&loop=1';
          iframeEl.src = a.href;
        });

        leftQtrContent.innerHTML = currentJSONobject.content[i].panel_type_id == 2 ? currentJSONobject.content[i].content : leftQtrContent.innerHTML;
        topRightContent.innerHTML = currentJSONobject.content[i].panel_type_id == 5 ? currentJSONobject.content[i].content : topRightContent.innerHTML;
        bottomRightContent.innerHTML = currentJSONobject.content[i].panel_type_id == 7 ? currentJSONobject.content[i].content : bottomRightContent.innerHTML;

        rightColumnQtr.style.display = "block";
        leftColumnQtrHalf.style.display = "block";

        fullColumn.style.display = "none";
        leftColumn.style.display = "none";
        rightColumn.style.display = "none";
        leftColumnQtr.style.display = "none";
        rightColumnQtrHalf.style.display = "none";

    }

}

console.log(pages_array[counter]);
console.log(setInterval);

setTimeout(reloadFunction, parseInt(pages_array[counter].duration) * 1000);

counter += 1; 
if (counter === pages_array.length){
    counter = 0; 
}

} 
console.log(obj);
console.log(pages_array);
</script>


<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>

 <script src="js/clock.js"></script>
 <script type="js/cycle.js"></script>

 <script type="text/javascript">
    $('.marquee').marquee({
        duration: 30000,
        gap:40,
    });
 </script>

<script type="text/javascript">
$(document).ready(function() {

  var feed = new Instafeed({
    get: 'user',
    userId: '5590008752',
    clientId: 'e228035bad48471a94244cc6305cf827',
    accessToken: '5590008752.e228035.07bcba1d14024e8c82081c50e59023e5',
    // template: '<a class="gallery-item" target="_blank" href="{{link}}"><img class="image_img" src="{{image}}" /><div class="caption_layer"><p class="instaCaption">{{model.likes.count}} Likes</p></div></a>',
    template: '<a target="_blank" href="{{link}}"><img src="{{image}}" /></a>',
    sortBy: 'most-recent',
    limit: '4',
    // template: '<div class="wrap"><img src="{{image}}"/><div class="caption">{{caption}}</div></div>',
    useHttp: true
    });

  $('#showMore').on('click', function() {
     $("#instafeed").fadeTo("slow", 0.33, function() {
        $(this).css('opacity','1');
        $(this).empty(); // remove this line if you want to keep previously loaded images on the page
        feed.next();
     })
});

  feed.run();

    // RegEx Source:  https://stackoverflow.com/a/37704433/362536
  const youtubeUrlPattern = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;

  [...document.querySelectorAll('iframe')].filter((iframeEl) => {
    // Filter to iframes loading YouTube URLs only.
    return iframeEl.src.match(youtubeUrlPattern);
  }).forEach((iframeEl) => {
    const a = document.createElement('a');
    a.href = iframeEl.src;
    a.search = a.search || '?';
    a.search += '&autoplay=1';
    iframeEl.src = a.href;
  });
});
</script>

<script type="text/javascript">
  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=2691675043";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

</body>
</html>
