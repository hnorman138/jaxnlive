<!DOCTYPE html>
<html>
<head>
	<title>Template One</title>

	<style type="text/css">
		html,
		body {
		  height: 100vh;
		  width: 100vw;
		  overflow: hidden;
		}

		.my-container {
		  display: flex;
		  flex-direction: column;
		  height: 100vh;
		  width:100vw;
		}

		.my-container>.top [class^="col-"],
		.my-container>.bottom [class^="col-"] {
		  background-color: #778899  ;
		  color: white;
		  text-align: center;
		}

		.my-container>.middle {
		  flex-grow: 1;
		  padding:30px;
		  background-image: url('images/bg_spring.svg');
		  background-size: cover;
		}

		.my-container>.middle>* {
		}

		#clock{
		    /*background-color:#333;*/
		    font-family: sans-serif;
		    font-size:40px;
		    text-shadow:0px 0px 1px #fff;
		    color:#fff;
		}
		#clock span {
		    color:#fff;
		    font-size:40px;
		    position:relative;

		}
		#date {
			margin-top: -10px;
		    letter-spacing:3px;
		    font-size:20px;
		    font-family:arial,sans-serif;
		    color:#fff;
		}
	</style>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" crossorigin="anonymous">
	<link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	     
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
	<script type='text/javascript' src='//cdn.jsdelivr.net/jquery.marquee/1.4.0/jquery.marquee.min.js'></script>

</head>
<body onload="startTime()">
<div class="container-fluid my-container">
	<div class="row top">
		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-3">
					<img style="height: 100px;width: 250px;" src="/images/jacksonLogoGRN.svg">
				</div>
				<div class="col-lg-6">
					<a class="weatherwidget-io" href="https://forecast7.com/en/35d16n84d88/cleveland/?unit=us" data-label_1="CLEVELAND" data-label_2="WEATHER" data-days="3" data-theme="original" data-basecolor="" >CLEVELAND WEATHER</a>
				</div>
				<div class="col-lg-3 align-self-center">
					<div id="clockdate">
						<div class="clockdate-wrapper">
							<div id="clock"></div>
							<div id="date"></div>
						</div>
					</div>			
				</div>
			</div>
		</div>
	</div>

	<?php 
	$leftContent = "Testing Left Widget";
	$rightContent = 'images/pennington.jpg';
	$youtube = 'https://www.youtube.com/embed/Lh9hA_3S8l4?autoplay=1';		 
	?>
	

	<div class="row middle">
		<div class="col-lg-6" style="padding:0;">
			<div style="background-color: white; max-height: 85%; max-width:85%; "> <img src="images/chair.jpg" class="img-fluid"></div>
		</div>
		<div class="col-lg-6"  >
<!-- 			<div style="background-color: white; height:auto"><img src="<?php echo $rightContent ?>" class="img-fluid" style="margin:0 auto;"></div>
 -->	<div style="height: 100%;"><iframe width="100%" height="100%" src="<?php echo $youtube ?>"  allow="autoplay; encrypted-media"></iframe></div>
		</div>
	</div>

	<div class="row bottom">
		<div class="col-lg-12">
			<div class="marquee"><h2 style="letter-spacing: 3px;">Happy Birthday/Work Anniversary!<span style=" color: #A9BD50; margin-right: 80px; margin-left: 80px;">|</span>	 Congratulations to {name} for winning last week's sales competition!<span style=" color: #A9BD50; margin-right: 80px; margin-left: 80px;">|</span>Weather Notice:</h2></div>
		</div>
	</div>
</div>


<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>

 <script src="js/clock.js"></script>

 <script type="text/javascript">
 	$('.marquee').marquee({
 		duration: 30000,
 		gap:30,
 	});
 </script>


</body>
</html>