<?php 
session_start();
require_once 'ti.php' 

?>

<!DOCTYPE html>
<html>
<head>
	<!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style.css">
    <!--Font Awesome CDN-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</head>
<body>
<header>
    <div class="branding">
        <div class="menu-button menu-toggle nav">
            <i class="material-icons">menu</i>
        </div>
        <img src="images/jacksonLogoGRN.svg"  />
    </div>
    <div class="page-details">
    </div>
    <div class="settings">
        <span class="userName">Hello, <?php echo $_SESSION['username']?></span>

        <div class="dropdown">
              
            <div class="menu-button profile dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="far fa-user-circle fa-2x"></i>
            </div>

            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="logout.php">Logout</a></li>
              </ul>

            <div class="menu-button menu-toggle aside">
                <i class="far fa-user-circle fa-2x"></i>
            </div>
        </div>
    </div>

</header>
<div class="app">
    <nav>
        <div class="title-block">
            <img src="images/jacksonLogoGRN.svg" />
        </div>
        <ul id="navGroup">
            <li><a href="index.php"><i class="fas fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
            </li>
            <li><a href="displays.php"><i class="fas fa-tv"></i>
                <span>Displays</span></a>
            </li>
            <li class="active">
                <a href="#pageSubmenu" data-toggle="collapse" data-parent="#navGroup" aria-expanded="false">
                    <i class="fas fa-file-alt"></i>
                    Pages
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li><a href="createpage.php">New Page</a></li>
                    <li><a href="pages.php">Pages</a></li>
                </ul>
            </li>
            <!-- <li class="active">
                <a href="#panelSubmenu" data-toggle="collapse" data-parent="#navGroup" aria-expanded="false">
                    <i class="fas fa-columns"></i>
                    Panels
                </a>
                <ul class="collapse list-unstyled" id="panelSubmenu">
                    <li><a href="createpanel.php">New Panel</a></li>
                    <li><a href="panels.php">Panels</a></li>
                </ul>
            </li> -->
            <li>
                <a href="contentLibrary.php">
                    <i class="fas fa-folder-open"></i>
                    Content
                </a>
            </li>
            <li>
                <a href="newsTickers.php">
                    <i class="fas fa-newspaper"></i>
                    News Tickers
                </a>
            </li>
        </ul>
        <hr>
        <div class="socialWrapper">
            <ul class="list-unstyled social">
                <li><a href="https://www.facebook.com/BuiltByJackson/" target="_blank" class="fab fa-facebook-square fa-lg"></a></li>
                <li><a href="https://www.instagram.com/builtbyjackson/" target="_blank" class="fab fa-instagram fa-lg"></a></li>
                <li><a href="https://twitter.com/BuiltByJackson" target="_blank" class="fab fa-twitter fa-lg"></a></li>
                <li><a href="https://www.pinterest.com/jacksonstyle/" target="_blank" class="fab fa-pinterest fa-lg"></a></li>
            </ul>
        </div>
    </nav>
    <article id="article">
            <?php startblock('article') ?>
            <?php endblock() ?>
    </article>
</div>



<script type="text/javascript">
$(document).ready(function() {
    $(".menu-toggle").on("click", function(e) {
        if($(this).hasClass("nav")) {
            $("nav").addClass("open");
        }
        else {
            $("aside").addClass("open");
        }
        e.stopPropagation();
    });
    
    $("body:not(nav)").on("click", function(e) {
        $("nav, aside").removeClass("open");
    });
});
</script>
<script>
   window.FontAwesomeConfig = {
      searchPseudoElements: true
   }
</script>
<script type="text/javascript">

</script>
</body>
</html>

