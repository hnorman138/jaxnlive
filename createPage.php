<!DOCTYPE html>
<html>
<head>
  <title>Page Templates</title>

  <style type="text/css" media="all">
  @import "style.css";
  </style>

  <!--Bootstrap CSS-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<body>

<?php include("menu.php");?>

<?php startblock('article') ?>

<h2>Choose A Template</h2>

<!-- Row One -->
<div class="row">
	<div class="col-sm-6" style="padding: 90px;"> 
		<div class="boxborder">
			<a href="templates.php?value=1"><h5 style="padding-bottom: 10px;">Template One</h5></a>
			<div class="container-fluid" style="text-align: center;">
				<div class="row">
					<div class="col-lg-12" style=" height:50px; background-color: #A0A0A0">
						<p style="color: white">Banner</p>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12" style="height: 350px; border:12px solid #b7c86c ; background-color: #A0A0A0;">
						<p style="color: white">Full Width</p>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12" style=" height:35px; background-color: #A0A0A0">
						<p style="color: white">Ticker</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6" style="padding: 90px;">
		<div class="boxborder">
			<a href="templates.php?value=2"><h5 style="padding-bottom: 10px;">Template Two</h5></a>
			<div class="container-fluid" style="text-align: center;">
				<div class="row">
					<div class="col-lg-12" style=" height:50px; background-color: #A0A0A0">
						<p style="color: white">Banner</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6" style=" height:350px; border:12px solid #b7c86c ; background-color: #A0A0A0">
						<p style="color: white">Half-width</p>
					</div>
					<div class="col-lg-6" style=" height:350px; border:12px solid #b7c86c ; background-color: #A0A0A0">
						<p style="color: white">Half-width</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12" style=" height:35px; background-color: #A0A0A0">
						<p style="color: white">Ticker</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Row Two -->
<div class="row">
	<div class="col-sm-6" style="padding: 90px;">
		<div class="boxborder">
			<a href="templates.php?value=3"><h5 style="padding-bottom: 10px;">Template Three</h5></a>
			<div class="container-fluid" style="text-align: center;">
				<div class="row">
					<div class="col-lg-12" style=" height:50px; background-color: #A0A0A0">
						<p style="color: white">Banner</p>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-12" style= " height:175px; border:12px solid #b7c86c ; background-color: #A0A0A0">		
								<p style="color: white">Quarter-width</p>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12" style= "height:175px; border:12px solid #b7c86c ; background-color: #A0A0A0">
								<p style="color: white">Quarter-width</p>
							</div>
						</div>
					</div>
					<div class="col-lg-6" style=" height:350px; border:12px solid #b7c86c ; background-color: #A0A0A0">	
						<p style="color: white">Half-width</p>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12" style=" height:35px; background-color: #A0A0A0">
						<p style="color: white">Ticker</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6" style="padding: 90px;"> 
		<div class="boxborder">
			<a href="templates.php?value=4"><h5 style="padding-bottom: 10px;">Template Four</h5></a>
			<div class="container-fluid" style="text-align: center;">
				<div class="row">
					<div class="col-lg-12" style=" height:50px; background-color: #A0A0A0">
						<p style="color: white">Banner</p>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6" style=" height:350px; border:12px solid #b7c86c ; background-color: #A0A0A0">	
						<p style="color: white">Half-width</p>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-12" style= " height:175px; border:12px solid #b7c86c ; background-color: #A0A0A0">
								<p style="color: white">Quarter-width</p>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12" style= "height:175px; border:12px solid #b7c86c ; background-color: #A0A0A0">
								<p style="color: white">Quarter-width</p>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12" style=" height:35px; background-color: #A0A0A0">
						<p style="color: white">Ticker</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php endblock() ?>

</body>
</html>