<?php include 'getPages.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<title>Template One</title>

	<style type="text/css">
		@import "style.css";
		html,
		body {
		  height: 100%;
		  overflow: hidden;
		}

		.my-container {
		  display: flex;
		  flex-direction: column;
		  height: 100%;
		}

		.my-container>.top [class^="col-"],
		.my-container>.bottom [class^="col-"] {
		  background-color: #929292  ;
		  color: white;
		  text-align: center;
		}

		.my-container>.middle {
		  flex-grow: 1;
		  padding:30px;
		  background-image: url('images/bg6.jpg');
		  background-size: cover;
		}

		.my-container>.middle>* {
		}

		#clock{
		    /*background-color:#333;*/
		    font-family: sans-serif;
		    font-size:40px;
		    text-shadow:0px 0px 1px #fff;
		    color:#fff;
		}
		#clock span {
		    color:#fff;
		    font-size:40px;
		    position:relative;

		}
		#date {
			margin-top: -10px;
		    letter-spacing:3px;
		    font-size:20px;
		    font-family:arial,sans-serif;
		    color:#fff;
		}

		.box-image {
		  width: 20%;
		  float: left;
		  box-sizing: border-box;
		  position: relative;
		  display: flex;
		  justify-content: center;
		  align-items: center;
		}

		.box-image:nth-child(6n+6) {
		  clear: left;
		}

		.box-image img {
		  width: 100%;
		  max-width: 150px;
		}

		.box-image a {
		  display:none;
		  position: absolute;  
		  
		  text-decoration: none;
		}

		.box-image:hover a {
		  display:block;
		  position: absolute;  
		  
		  text-decoration: none;
		  background-color: white;
		  
		}

		
	</style>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" crossorigin="anonymous">
	<link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	     
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
	<script type='text/javascript' src='//cdn.jsdelivr.net/jquery.marquee/1.4.0/jquery.marquee.min.js'></script>

</head>
<body>


<?php include("menu.php");?>

<?php startblock('article') ?>



<div class="container-fluid">
	<div class="row" style="width: 100%; padding-bottom: 20px;">
		<div class="col-lg-12">
			<div class="page-header">
				<h2>Images</h2>
			</div>
			<div class="row">
				<div class="col-lg-8">
				<div class="row" style="margin-bottom:10px;">
				<?php foreach($imageResult as $im):?>
					<?php if ($im['type'] == 'content'){?>
				<div class="box-image" style="margin-bottom:10px;">
				<img class="contentImage" src="<?php echo $im['url'] ?>" style="max-width: 150px; height: auto;">
				<a href="deleteImage.php?imageID=<?php echo $im['ID']?>">Delete</a> 
				</div>
			<?php } ?>
				<?php endforeach?>
				</div>
				</div>
				<div class="col-lg-4">
					<form action="uploadImage.php" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="formControl">Upload Image</label>
							<input type="file" name="file" class="form-control-file" id="formControl">
							<input type="submit" name="submit">
						</div>
					</form>
				</div>
				
			</div>
			
		</div>
		
	</div>

	<div class="row" style="width: 100%; padding-bottom: 20px;">
		<div class="col-lg-12">
			<div class="page-header">
				<h2>Background Images</h2>
			</div>
			<div class="row">
				<div class="col-lg-8">
				<div class="row" style="margin-bottom:10px;">
				<?php foreach($imageResult as $im):?>
					<?php if ($im['type'] == 'background'){?>
				<div class="box-image" style="margin-bottom:10px;">
				<img class="contentImage" src="<?php echo $im['url'] ?>" style="max-width: 150px; height: auto;">
				<a href="deleteImage.php?imageID=<?php echo $im['ID']?>">Delete</a> 
				</div>
			<?php } ?>
				<?php endforeach?>
				</div>
				</div>
				<div class="col-lg-4">
					<form action="uploadBackgroundImage.php" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="formControlBG">Upload Image</label>
							<input type="file" name="fileBG" class="form-control-file" id="formControlBG">
							<input type="submit" name="submitBG">
						</div>
					</form>
				</div>
				
			</div>
			
		</div>
		
	</div>

	<div class="row" style="width: 100%; padding-bottom: 20px; padding-top: 20px;">
		<div class="col-lg-12">
			<div class="page-header">
				<h2>Widgets</h2>
			</div>
			<div class="row">
				<div class="col-lg-12">
					Widget Gallery - Coming Soon
				</div>
				<div>
				
				</div>
			</div>
			
		</div>
		
	</div>
	
</div>

<?php endblock() ?>

</body>
</html>