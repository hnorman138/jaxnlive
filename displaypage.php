<?php include 'getPages.php';?>

<!DOCTYPE html>
<html>
<head>

    <style type="text/css" media="all">
  @import "style.css";
  </style>

  <!--Bootstrap CSS-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>

</head>

<body>

<?php include("menu.php");?>
<?php startblock('article') ?>
<!-- CONTAINER -->
<div class="container-fluid" style="margin-bottom: 20px;">
	<div class="row" style="width: 100%; padding-bottom: 20px;">
    <div class="col-sm-12">
        
        <?php foreach($displayNameResult as $names): ?>
        <div class="page-header"><h2><?php echo $names['location_name']?> - <?php echo $names['display_name']; ?></h2><a href="showDisplay.php?display=<?php echo $names['DisplayID'];?>" target="_blank">View Display</a>
          <a style="margin-left:10px;" href="" data-toggle="modal" data-target="#assignPageModal">Assign New Page</a></div>

          <div class="modal fade" id="assignPageModal" tabindex="-1" role="dialog" aria-labelledby="assignPageLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="assignPageLabel">Assign Page</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h5>Choose page to assign to this display:</h5>
              <?php //echo $_GET['displayID'];?>
              <select name="pageToAssign"  id="pageToAssign">
                <option value="" disabled selected>Select page to assign...</option>
                <?php foreach($activePages as $pages):?>
                  <option value="<?php echo $pages['id']?>"><?php echo $pages['title']?></option>
                <?php endforeach?>
              </select>
              <a href="assignPage.php?pageID=<?php echo $pages['id']?>&displayID=<?php echo $_GET['displayID']?>" id="assignPage" class="btn btn-primary" role="button"> Assign Page</a>

              <script type="text/javascript">
                document.getElementById('pageToAssign').addEventListener('change', function() {
                  var assignPage = document.getElementById('assignPage');
                  var href = assignPage.getAttribute('href');
                  
                  assignPage.href = href.replace( /pageID=[^&]+/, 'pageID='+ this.value );
                });
              </script>

            </div>

          </div>
        </div>
      </div>
        <?php endforeach;?>

        

      <div class="row">
        <?php $count = 0;?>
        <?php foreach ($displayPageResult as $pages): ?>
          <div class="col-lg-3 col-sm-6" style="padding-bottom: 20px;">
          <div style="padding-bottom:20px;" class="card text-center">
          <h4 class="card-title"><?php echo $pages['title']; ?></h4>
          <a href="showpage.php?pageid=<?php echo $pages['page_id']?>" target="_blank">View Page</a>
          <a href="" data-toggle="modal" data-target="#deletePage<?php echo $count; ?>">Delete Page</a>
      </div>
    </div>

    <!--Modal for changing duration-->
      <div class="modal fade" id="changePosition<?php echo $count; ?>" tabindex="-1" role="dialog" aria-labelledby="changePositionLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="changePositionLabel">Enter new position(1,5,10,etc.):</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="changePosition.php?displayID=<?php echo $_GET['displayID']?>" method="post">
                
                <input type="text" name="position">
                <input type="hidden" name="pageID" value="<?php echo $pages['ID']?>">

                <input type="submit" value="Update">
              </form>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal to confirm page deletion -->
      <div class="modal fade" id="deletePage<?php echo $count; ?>" tabindex="-1" role="dialog" aria-labelledby="deletePageLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="deletePageLabel">Are you sure you want to delete this page?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <!-- <form method="post"> -->
              <!-- <input type="text" name="confirmDelete"> -->
              <?php echo $pages['page_id'];?>
              <a href="deletePageFromDisplay.php?deleteID=<?php echo $pages['page_id']?>&displayID=<?php echo $_GET['displayID']?>" class="btn btn-primary" role="button">Delete</a>
              <button class="btn btn-secondary" role="button" data-dismiss="modal">Cancel</button>
              <!-- </form> -->
            </div>

          </div>
        </div>
      </div>
      <?php $count++;?>
        <?php endforeach; ?>
      </div>

    </div>
    <!-- END FIRST ROW -->
  </div>
</div>

<?php endblock() ?>
</body>
</html>