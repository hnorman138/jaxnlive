<?php include '../getpages.php'?>

<div class="row top">
	<?php include 'banner.php'?>
</div>

<div class="row middle">
	<div class="col-lg-12 fullWidth">
		<div class="fullContent" style="background-color: white; height: 100%;">
			
		</div>

	</div>
</div>

<div class="modal fade bd-example-modal-lg" id="fullModal" tabindex="-1" role="dialog" aria-labelledby="fullLabel" aria-hidden="true">
			    <div class="modal-dialog modal-lg" role="document">
			      	<div class="modal-content">
				        <div class="modal-header">
				          <h3 class="modal-title" id="fullModal">Content Library:</h3>
				         
				          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				            <span aria-hidden="true">&times;</span>
				          </button>
				        </div>
				        <div class="modal-body">
				        	<?php foreach ($imageResult as $im): ?>
				        		<?php if($im['type'] == 'content'){?>
				          	<img src="<?php echo $im['url']; ?>" style="max-width:200px; max-height:200px;">
				          <?php } ?>
				          <?php endforeach?>
				        	
				        	<h3>Create your own content</h3>
				        	<form  id="form-data3" method="post">
						      <textarea class="original" id="mytextarea3" name="fullText"></textarea>
						      <input type="submit" value="Save Content">
						    </form>
					    </div>
					    <div class="modal-footer">
					      <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					        <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button> -->
					    </div>
				   </div>
				</div>
			</div>

<div class="row bottom">
<?php include 'ticker.php';?>
</div>
