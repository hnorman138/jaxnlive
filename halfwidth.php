<?php include '../getpages.php'?>


<div class="row top">
	<?php include 'banner.php'?>
</div>

<div class="row middle">
	<div class="col-lg-6 leftFifty">
	  	<div class="leftContent" style="background-color: white; height: 100%; ">		
	  		<div class="modal fade bd-example-modal-lg" id="leftFiftyModal" tabindex="-1" role="dialog" aria-labelledby="leftFiftyLabel" aria-hidden="true">
			    <div class="modal-dialog modal-lg" role="document">
			      	<div class="modal-content">
				        <div class="modal-header">
				          <h5 class="modal-title" id="leftFiftyModal">Content Library:</h5>
				          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				            <span aria-hidden="true">&times;</span>
				          </button>
				        </div>
				        <div class="modal-body">
				        	<?php foreach ($imageResult as $im): ?>
				        		<?php if($im['type'] == 'content'){?>
				          	<img src="<?php echo $im['url']; ?>" style="max-width:200px; max-height:200px;">
				          <?php } ?>
				          <?php endforeach?>
				        	<hr>
				        	<h3>Create your own content</h3>
				        	<form id="form-data" method="post">
						      <textarea class="original" id="mytextarea"></textarea>
						      <input type="submit" value="Save Content">
						    </form>
					    </div>
					    <div class="modal-footer">
					        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					        <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button> -->
					    </div>
				   </div>
				</div>
			</div>
	  	</div>
	</div>
	<div class="col-lg-6 rightFifty">
	  <div class="rightContent" style="background-color: white; height: 100%; ">
	  	<div class="modal fade  bd-example-modal-lg" id="rightFiftyModal" tabindex="-1" role="dialog" aria-labelledby="rightFiftyLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg" role="document">
		      <div class="modal-content">
		        <div class="modal-header">
		          <h5 class="modal-title" id="rightFiftyModal">Content Library:</h5>
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		        </div>
		        <div class="modal-body">
		        	<h3>Images</h3>
		        	<?php foreach($bodyImg as $bImg) : ?>
		        		<img style="width: 200px; height: 200px;" src="<?php echo $bImg?>">
		        	<?php endforeach?>
		        	<hr>
		        	<h3>Create your own content</h3>
		        	<form id="form-data2" method="post">
				      <textarea id="mytextarea2"></textarea>
				      <input type="submit" value="Get Data">
				    </form>
			    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
				    </div>
			   </div>
			</div>
		</div>
	  </div>
	</div>
</div>

<div class="row bottom">
	<?php include 'ticker.php';?>
</div>