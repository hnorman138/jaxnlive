<div class="col-lg-12">
	<div class="row" style="background-color: #778899;">
		<div class="col-lg-3">
			<img style="height: 100px;width: 250px;" src="/images/jacksonLogoGRN.svg">
		</div>
		<div class="col-lg-6">
			<?php if(isset($_GET['display']))
			{?>
				<?php if($_GET['display'] == 18){ ?>
					<a class="weatherwidget-io" href="https://forecast7.com/en/38d92n78d19/front-royal/?unit=us" data-label_1="FRONT ROYAL" data-label_2="WEATHER" data-days="3" data-theme="original" data-basecolor="" " >FRONT ROYAL WEATHER</a>
				<?php } elseif($_GET['display'] == 19){ ?>
					<a class="weatherwidget-io" href="https://forecast7.com/en/38d95n78d18/22630/?unit=us" data-label_1="LAKE FREDERICK" data-label_2="WEATHER"  data-days="3" data-theme="original" data-basecolor="" " >LAKE FREDERICK WEATHER</a>
				<?php } elseif($_GET['display'] == 16){ ?>
					<a class="weatherwidget-io" href="https://forecast7.com/en/34d34n88d50/38855/?unit=us" data-label_1="MANTACHIE" data-label_2="WEATHER"  data-days="3" data-theme="original" data-basecolor=""  >MANTACHIE WEATHER</a>
				<?php } elseif($_GET['display'] == 17) { ?>
					<a class="weatherwidget-io" href="https://forecast7.com/en/34d56n89d12/myrtle/?unit=us" data-label_1="MYRTLE" data-label_2="WEATHER"  data-days="3" data-theme="original" data-basecolor=""  >MYRTLE WEATHER</a>
				<?php } else { ?>
					<a class="weatherwidget-io" href="https://forecast7.com/en/35d16n84d88/cleveland/?unit=us" data-label_1="CLEVELAND" data-label_2="WEATHER" data-days="3" data-theme="original" data-basecolor="" >CLEVELAND WEATHER</a>
				<?php }?>

			<?php }?>
		</div>
		<div class="col-lg-3 align-self-center">
			<div id="clockdate">
				<div class="clockdate-wrapper">
					<div id="clock"></div>
					<div id="date"></div>
				</div>
			</div>			
		</div>
	</div>
</div>

<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>