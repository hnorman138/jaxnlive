<?PHP
require_once 'dbConnect.php';

// $getContent = "SELECT * 
// 				FROM contentmanagement.paget
// 				";

// try{
// 	$contentFetch = $DB2Conn->prepare($getContent);
// 	$contentResult = $contentFetch->execute();
// 	$fetchResult = $contentFetch->fetchAll(PDO::FETCH_ASSOC);
// } catch(PDOException $ex) {
// 	echo "QUERY FAILED: " .$ex->getMessage();
// }

// echo $contentResult;


$getDisplays = "
SELECT l.id , location_name, d.id as display_id, display_name, location_id
FROM jaxnlive.locations l
inner join jaxnlive.displays d
ON d.location_id = l.id
where d.active = 1";

$displayResult = $mysqlConn->query($getDisplays);

$displayNames = array();
foreach($displayResult as $subArray) {
    if(!array_key_exists($subArray['location_name'], $displayNames)) {
        $displayNames[$subArray['location_name']] = array();

    }
    // here you add `display_name` under key `display_id`
    $displayNames[$subArray['location_name']][$subArray['display_id']] = $subArray['display_name'];
}

//print_r($displayNames);

// foreach ($displayResult as $key => $value) {
// print_r($value);
// }

///////////////////////////////////////////////////////////////////

$displayID = $_GET['displayID'];

$displayPages = "
SELECT dp.id as ID, p.id as page_id, p.title, dp.display_id, p.active, p.position from jaxnlive.pages p
left join jaxnlive.displays d
on p.display_id = d.id
left join jaxnlive.locations l
on d.location_id = l.id
left join jaxnlive.display_to_page dp
on p.id = dp.page_id
where dp.active = 1
and p.active = 1
and p.start_time <= current_timestamp()
and p.end_time > curdate()
AND dp.display_id = '".$displayID."'
order by position asc
";

$displayPageResult = $mysqlConn->query($displayPages);


/////////////////////////////////////////////////////////

$displayEntities = "
SELECT d.id as DisplayID, display_name, location_id, l.id as locID, location_name from jaxnlive.displays d
inner join jaxnlive.locations l
on d.location_id = l.id
where d.id = '".$displayID."'
";

$displayNameResult = $mysqlConn->query($displayEntities);


//////////////////////////////////////////
$getContent = "
SELECT *
FROM jaxnlive.pages p
inner join jaxnlive.displays d
ON p.display_id = d.id
where p.active = 1
and p.start_time <= current_timestamp()
and p.end_time > current_timestamp()
order by p.position asc;";

$result = $mysqlConn->query($getContent);

//////////////////////////////////////

$id = $_GET['pageid'];

$getPage = "
SELECT *
FROM jaxnlive.pages p
where p.id ='".$id."'
";

$pageResult = $mysqlConn->query($getPage);

////////////////////////////////////////////////////
$getPanels = "
SELECT panel_type_id,content 
FROM jaxnlive.panels pn
inner join jaxnlive.content c
on pn.cont_id = c.id
where pn.active = 1
AND pn.page_id = '".$id."'

";

$panelResult = $mysqlConn->query($getPanels);
$data_array = array();
while ($panelRow = mysqli_fetch_assoc($panelResult)) {
    $data_array[$panelRow['panel_type_id']] = $panelRow['content'];
}


////////////////////////////////////////////////

$getImages = "
SELECT * 
FROM jaxnlive.images
WHERE active = 1;
";

$imageResult = $mysqlConn->query($getImages);


///////////////////////////////////////////////

/*GET LIVE DISPLAY PAGES*/

$display = $_GET['display'];

$getDisplayPage = "
SELECT p.id as pageID, page_type_id, dp.display_id, slide_order, duration, background_img, pn.ID as panel_id, panel_type_id, cont_id, c.ID as contID, content 
FROM jaxnlive.pages p
inner join jaxnlive.panels pn
on p.id = pn.page_id 
inner join jaxnlive.content c
on pn.cont_id = c.id
inner join jaxnlive.display_to_page dp
on p.id = dp.page_id
WHERE p.active = 1
and p.start_time <= current_timestamp()
and p.end_time > current_timestamp()
and pn.active = 1
and dp.active = 1
AND dp.display_id = '".$display."'
order by p.position asc
";

$showDisplayResult = $mysqlConn->query($getDisplayPage);
while($row=mysqli_fetch_assoc($showDisplayResult))
	{
		$rows[] = $row;
		//$rows[$row['pageID']][] = $row;
	}
$showDisplays = json_encode($rows,JSON_PRETTY_PRINT);


/////////////////

/*GET EXPIRED PAGES*/

$getExpiredPages = "
	SELECT p.id, title, display_id, end_time, active, d.id as display_id, display_name, location_id, location_name
		FROM jaxnlive.pages p
		inner join jaxnlive.displays d
			on p.display_id = d.id
		inner join jaxnlive.locations l
			on d.location_id = l.id
		WHERE end_time between NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY)
		AND active = 1;
";

$expiredPages = $mysqlConn->query($getExpiredPages);


////////////////////////////

$getTickers = "
	SELECT d.ID as displayID, d.display_name as display, l.location_name as locationName, d.location_id as location, t.id as ticker, tc.id as contentID, tc.content
		FROM jaxnlive.displays d
			INNER JOIN jaxnlive.locations l on d.location_id = l.id
			INNER JOIN jaxnlive.tickers t on d.id = t.display_id
			INNER JOIN jaxnlive.ticker_content tc on t.id = tc.ticker_id
		WHERE tc.active = 1;
";

$tickers = $mysqlConn->query($getTickers);

$tickerDisplays = [];

// Walk through the query result
foreach($tickers as $row) {
    $displayID = $row['displayID']; // for convenience and readability
    $location = $row['location'];   // for convenience and readability
    $displays = $row['display'];
    $contentID = $row['contentID'];

    if ( ! array_key_exists($row['displayID'], $tickerDisplays) ) {
        $tickerDisplays[$displayID] = [
            'displayID' => $row['displayID'],
            'display' => $row['display'],
            'ticker' => $row['ticker'],
            'contentID' => $row['contentID'],
            'content' => $row['content'],
            'location' => $row['location'],
            'locationName' => $row['locationName'],
            '@content' => [] // to store the content data                
        ];

    }
    $tickerDisplays[$displayID]['@content'][$contentID] = ['content' => $row['content'], 'contentID' => $row['contentID']];
}

///////////////////////////

$getTickerContent = "
	SELECT content
	 from jaxnlive.ticker_content c
	  inner join jaxnlive.tickers t
	   on c.ticker_id = t.id
	  inner join jaxnlive.displays d
	   on t.display_id = d.id
	 where c.active = 1
	 and d.id = '".$display."'
";

$tickerContent = $mysqlConn->query($getTickerContent);


///////////////////////////


$getDisplayByLocation = "
	SELECT l.id as locationID, location_name, d.id as displayID, display_name from jaxnlive.locations l
		inner join jaxnlive.displays d
		on l.id = d.location_id;
";

$displayLocation = $mysqlConn->query($getDisplayByLocation);

foreach ($displayLocation as $row) {
	
}

/////////////////////////////

$getActivePages = "
	SELECT id, title
		FROM jaxnlive.pages
		where active = 1
		and start_time <= current_timestamp()
		OR end_time > current_timestamp();
";

$activePages = $mysqlConn->query($getActivePages);

////////////////////////////

mysqli_close($mysqlConn);

?>