function startTime() {
    const urlParams = new URLSearchParams(window.location.search);
    const myParam = urlParams.get('display');

    var today = new Date();

    if (myParam == 16 || myParam == 17) {
        var hr = today.getHours()- 1;
    }else{
        var hr = today.getHours();
    }
    var min = today.getMinutes();
    // var sec = today.getSeconds();
    ap = (hr < 12) ? "<span>AM</span>" : "<span>PM</span>";
    hr = (hr == 0) ? 12 : hr;
    hr = (hr > 12) ? hr - 12 : hr;
    //Add a zero in front of numbers<10
    hr = checkTime(hr);
    min = checkTime(min);
    // sec = checkTime(sec);
    document.getElementById("clock").innerHTML = hr + ":" + min + " " + ap;
    
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var curWeekDay = days[today.getDay()];
    var curDay = today.getDate();
    var curMonth = months[today.getMonth()];
    // var curYear = today.getFullYear();
    var date = curWeekDay+", "+curDay+" "+curMonth;
    document.getElementById("date").innerHTML = date;
    
    var time = setTimeout(function(){ startTime() }, 500);
}
function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
