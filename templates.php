<?php include('getPages.php');?>

<!DOCTYPE html>
<html>
<head>
<title>Template One</title>

<style type="text/css">
@import "style.css";
html,
body {
  height: 100%;
  width:100%;
  overflow-y: scroll;
}

iframe{
	height:100% !important;
	width:100% !important;
}

.middle p{
max-height:100%;
margin-bottom:0;     
position: absolute; 
top:0; 
right:0; 
bottom:0; 
left:0;
display:flex;
}

img {
object-fit: scale-down;
width: 100%; 
margin:0 auto;
}

/*FULL CONTENT EDITOR*/
#fullContent{
display:flex;
justify-content:center;
align-items:center;
overflow: hidden;
}

#fullContent img{
object-fit: contain;
height:100%;
}

.fullContent {
max-width: 100%;
max-height: 100%;
height: auto;
width: auto;
}

.fullContent > img{
min-width:100%;
min-height: 100%;
width: auto;
height: auto;
}

/*QUARTER  and HALF CONTENT EDITOR*/
#leftContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#leftContent img{
    object-fit: contain;
}
.leftContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.leftContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#topRightContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#topRightContent img{
    object-fit: contain;
}
.topRightContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.topRightContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#bottomRightContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#bottomRightContent img{
    object-fit: contain;
}
.bottomRightContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.bottomRightContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#topLeftContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#topLeftContent img{
    object-fit: contain;
}
.topLeftContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.topLeftContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#bottomLeftContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#bottomLeftContent img{
    object-fit: contain;
}
.bottomLeftContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.bottomLeftContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}
#rightContent{
  display:flex;
  justify-content:center;
  align-items:center;
  overflow: hidden;
  height:100%;
}
#rightContent img{
    object-fit: contain;
}
.rightContent{
  max-width: 100%;
  max-height: 100%;
  height: auto;
  width: auto;
}
.rightContent> img{
  min-width:100%;
  min-height: 100%;
  width: auto;
  height: auto;
}



.my-container {
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 100vh;
  width:100vw;
}

.my-container .top.row, 
.my-container .row.bottom {
  flex-shrink: 0;
}


.my-container>.top [class^="col-"],
.my-container>.bottom [class^="col-"] {
  background-color: #778899  ;
  color: white;
  text-align: center;
}

.my-container>.middle {
  flex-grow: 1;
  padding:30px;
  background-image: url('images/bg6.jpg');
  background-size: cover;
}

.my-container>.middle>* {
}

#clock{
    /*background-color:#333;*/
    font-family: sans-serif;
    font-size:40px;
    text-shadow:0px 0px 1px #fff;
    color:#fff;
}
#clock span {
    color:#fff;
    font-size:40px;
    position:relative;

}
#date {
	margin-top: -10px;
    letter-spacing:3px;
    font-size:20px;
    font-family:arial,sans-serif;
    color:#fff;
}

.buttonContainer {
  display: flex;
  flex-direction: column;
  justify-content: center;
}
</style>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <link href="https://rawgit.com/tempusdominus/bootstrap-4/master/build/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/OwlCarousel/dist/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="/OwlCarousel/dist/assets/owl.theme.default.min.css">
  <link href="src/ytv.css" type="text/css" rel="stylesheet" />
    
<!-- 	  <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	  <script type="text/javascript" src="instafeed.js-master/instafeed.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.0/moment-with-locales.js"></script>
	  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	  <script src="https://rawgit.com/tempusdominus/bootstrap-4/master/build/js/tempusdominus-bootstrap-4.min.js"></script>
	  <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
	  	<script type='text/javascript' src='//cdn.jsdelivr.net/jquery.marquee/1.4.0/jquery.marquee.min.js'></script> -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="OwlCarousel/dist/owl.carousel.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
<script type='text/javascript' src='//cdn.jsdelivr.net/jquery.marquee/1.4.0/jquery.marquee.min.js'></script>

</head>
<body onload="startTime()">

<?php include("menu.php");?>

<?php startblock('article') ?>

	<!-- TITLE BLOCK -->
	<div class="container-fluid title-container">
		<div class="row">
			<div class="col-lg-12" style="text-align: center;">
				<div class="page-header">
	        	<h2>Create A New Page</h2>      
	      		</div>
	      	</div>
      	</div>
      	
      	<br>
	</div>
	<hr>

	<div class="container-fluid my-container">
		<!-- Background image modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h5 class="modal-title" id="exampleModalLabel">Choose an image:</h5>
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	          </button>
	        </div>
	        <div class="modal-body">

	        	<?php foreach($imageResult as $im): ?>
	        		<?php if($im['type'] == 'background' ) {?>
		        <img style="width: 200px; height: 200px;" src="<?php echo $im['url'] ?>">
		    	<?php } ?>
		    	<?php endforeach ?>
		      </div>
				<form  id="bgForm" method="post">
	              <input type="hidden" name="page_content" id="bgContent" value="">
	          	</form>
		    </div>
		  </div>
		</div>






	  	<?php $value = isset($_GET['value']) ? $_GET['value'] : 1;?>
	  	<!-- Full width -->
	  	<?php if($value == 1){?>
	  		<?php $panelType = 1;?>
	  	<?php include 'class/fullWidth.php';?>

	
	    <!-- quarter-right -->
	    <?php }elseif($value == 3 || $value == 4){ ?>

	    <?php include 'class/quarterwidth.php' ?>
	    
	    <!--Half Width Template-->
	    <?php }else{?>
	    	<?php //$panelType = 2;?>
	    <?php include 'class/halfWidth.php'?>
		<?php } ?>

	<div class="row" style="margin-top:20px;">
      		<div class="col-lg-6">
      			<a href="#exampleModal" data-toggle="modal" data-target="#exampleModal">Choose Page Background</a>
      		</div>
      		<div class="col-lg-6">
      			<button id="setActivePage" style="float: right;">Save Page</button>
      		</div>
  	</div>
	</div>

	
 	<!-- <button onclick="content()" data-toggle="modal" data-target="#my_modal">Save</button> -->

	<!-- Save Page Modal -->
	<form data-toggle="validator" method="post" id="pageForm">
	<div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-labelledby="savePageLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h5 class="modal-title" id="savePageLabel">Page Details:</h5>
	          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	          </button>  -->
	        </div>
	        <div class="modal-body">
	        	<div class="form-group has-error">
	        		
	        		<input type="hidden" name="pageType" value="<?php echo $value;?>">
	        		</br>
	        		<label for="addTitle">Page Title:</label>
	        		<input class="form-control required" id="addTitle" name="addTitle" required=true>

	        		</input>

	        		<!-- <label for="plantSelect">Select A Location</label>
	        		<select name="" class="form-control" id="plantSelect">
	        			<?php //foreach($displayNames as $key => $displayName):?>
						<option value=""><?php //echo $key; ?></option>
						<?php //endforeach;?>
	        		</select> -->

	        		<label for="areaSelect">Select An Area</label>
	        		<select name="displayId" class="form-control" id="areaSelect">
	        			<option value="" disabled selected>Select one...</option>
	        			 <?php foreach($displayNames as $key => $displayName):?>
	        				<optgroup label="<?php echo $key?>">
	        					<?php foreach($displayName as $key2 => $displayNameRoom): ?>
	        					<option value="<?php echo $key2?>"><?php echo $displayNameRoom?></option>
	        				<?php endforeach?>
        				<?php endforeach?>


	        		</select>

	        		<label for="durationSet">Set A Duration (in seconds)</label>
	        		<input class="form-control" id="durationSet" name="durationSet">

	        		</input>

	        		<label for="startDateSelect">Set Start Date/Time</label>
	        		<div class="form-group">
                      <div class="datepick input-group date" id="datetimepickerStart" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepickerStart" name="datePickerStart" />
                        <span class="input-group-addon" data-target="#datetimepickerStart" data-toggle="datetimepicker">
                        <span class="fa fa-calendar"></span>
                        </span>
                      </div>
                    </div>

	        		<label for="expirationSelect">Set Expiration Date/Time</label>
	        		<div class="form-group">
                      <div class="datepick input-group date" id="datetimepicker" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker" name="datePicker" />
                        <span class="input-group-addon" data-target="#datetimepicker" data-toggle="datetimepicker">
                        <span class="fa fa-calendar"></span>
                        </span>
                      </div>
                    </div>
	        	</div>
		    </div>
		      <div class="modal-footer">
		        <input type="submit" name="Save Page" id="submitForm">
		      </div>
		    </div>
		  </div>
		</div>
	</div>
	</form>


<!-- Call modal on pageload, require form before closing/submitting -->
<script type="text/javascript"> 
$(window).on('load',function(){
        $("#my_modal").modal({
            backdrop: 'static',
	        keyboard: false,
	        show: true
        });
});
</script>

<!-- INitialize Modal, not allowing close if clicking outside modal -->
<script type="text/javascript">
$(document).ready(function(){
	$(".show-modal").click(function(){
		$("#pageForm").modal({
			modal-backdrop:'static',
			keyboard:false
		});
	});
});
</script>

<!-- AJAX for Initial page insert -->
<script type="text/javascript">

var page_id;

$(document).ready(function(){
$("#submitForm").click(function(){
event.preventDefault();
var string = $('#pageForm').serialize();

	// AJAX Code To Submit Form.
	    $.ajax({
	        type: "POST",
	        url: "addPage.php",
	        data: string,
	        dataType: 'json',
	        cache: false,
	        success: function(response){
	          console.log(string);
	          console.log(response['last_insert_id']);

	          page_id = JSON.stringify(response['last_insert_id']);

	          $('#my_modal').modal('hide');
	          $('.modal-backdrop').remove();
	        }
	    });
	});
});
</script>

<!-- AJAX for Left panel insert -->
<script type="text/javascript">

$("#leftHalfForm").submit(function(e){

    var leftContentVar = tinymce.get("leftHalfTextArea").getContent();

    $(".leftContent").html(leftContentVar);
    $("#leftHalfPageContent").val(leftContentVar);

    // jQuery.noConflict();

      var string = $('#leftHalfForm').serialize() + '&page_id=' + page_id;
      console.log(string);


	// AJAX Code To Submit Form.
	    $.ajax({
	        type: "POST",
	        url: "addPanel.php",
	        data: string,
	        cache: false,
	        success: function(response){
	          console.log(JSON.stringify(response));
	          console.log(string);


	          $('#leftFiftyModal').modal('hide');
	          $('.modal-backdrop').remove();
	        }
	    });

    return false;

});


</script>

<!-- AJAX for Right panel insert -->

<script type="text/javascript">
    $(document).ready(function(){

    $("#rightHalfForm").submit(function(e){

        var rightContentVar = tinymce.get("rightHalfTextArea").getContent();

        $(".rightContent").html(rightContentVar);
        $("#rightHalfPageContent").val(rightContentVar);

        // jQuery.noConflict();

          var string = $('#rightHalfForm').serialize() + '&page_id=' + page_id;

		// AJAX Code To Submit Form.
		    $.ajax({
		        type: "POST",
		        url: "addPanel.php",
		        data: string,
		        cache: false,
		        success: function(response){
		          console.log(JSON.stringify(response));

		          $('#rightFiftyModal').modal('hide');
		          $('.modal-backdrop').remove();
		        }
		    });

        return false;

    });

});
</script>

<!-- AJAX INSERT for full panel insert -->
<script type="text/javascript">
    $(document).ready(function(){

    $("#fullForm").submit(function(e){

        var content3 = tinymce.get("fullTextArea").getContent();

        $(".fullContent").html(content3);
        $("#fullPageContent").val(content3);


        //jQuery.noConflict();

        var string = $('#fullForm').serialize() + '&page_id=' + page_id;

		// AJAX Code To Submit Form.
		    $.ajax({
		        type: "POST",
		        url: "addPanel.php",
		        data: string,
		        cache: false,
		        success: function(response){
		          console.log(JSON.stringify(response));

		          $('#fullModal').modal('hide');
		          $('.modal-backdrop').remove();
		        }
		    });

        // $('#fullModal').modal('hide');
        // $('.modal-backdrop').remove();

        return false;

    });

});
</script>

<!-- AJAX INSERT for top left panel insert -->
<script type="text/javascript">
    $(document).ready(function(){

    $("#topLeftForm").submit(function(e){

        var topLeftContentVar = tinymce.get("topLeftTextArea").getContent();

        $(".topLeftContent").html(topLeftContentVar);
        $("#topLeftPageContent").val(topLeftContentVar);


        //jQuery.noConflict();

        var string = $('#topLeftForm').serialize() + '&page_id=' + page_id;

		// AJAX Code To Submit Form.
		    $.ajax({
		        type: "POST",
		        url: "addPanel.php",
		        data: string,
		        cache: false,
		        success: function(response){
		          console.log(JSON.stringify(response));

		          $('#topLeftModal').modal('hide');
		          $('.modal-backdrop').remove();
		        }
		    });


        return false;

    });

});
</script>

<!-- AJAX INSERT for top right panel insert -->
<script type="text/javascript">
    $(document).ready(function(){

    $("#topRightForm").submit(function(e){

        var topLeftContentVar = tinymce.get("topRightTextArea").getContent();

        $(".topRightContent").html(topLeftContentVar);
        $("#topRightPageContent").val(topLeftContentVar);


        //jQuery.noConflict();

        var string = $('#topRightForm').serialize() + '&page_id=' + page_id;

		// AJAX Code To Submit Form.
		    $.ajax({
		        type: "POST",
		        url: "addPanel.php",
		        data: string,
		        cache: false,
		        success: function(response){
		          console.log(JSON.stringify(response));

		          $('#topRightModal').modal('hide');
		          $('.modal-backdrop').remove();
		        }
		    });


        return false;

    });

});
</script>


<!-- AJAX INSERT for bottom left panel insert -->
<script type="text/javascript">
    $(document).ready(function(){

    $("#bottomLeftForm").submit(function(e){

        var topLeftContentVar = tinymce.get("bottomLeftTextArea").getContent();

        $(".bottomLeftContent").html(topLeftContentVar);
        $("#bottomLeftPageContent").val(topLeftContentVar);


        //jQuery.noConflict();

        var string = $('#bottomLeftForm').serialize() + '&page_id=' + page_id;

		// AJAX Code To Submit Form.
		    $.ajax({
		        type: "POST",
		        url: "addPanel.php",
		        data: string,
		        cache: false,
		        success: function(response){
		          console.log(JSON.stringify(response));

		          $('#bottomLeftModal').modal('hide');
		          $('.modal-backdrop').remove();
		        }
		    });


        return false;

    });

});
</script>

<!-- AJAX INSERT for bottom right panel insert -->
<script type="text/javascript">
    $(document).ready(function(){

    $("#bottomRightForm").submit(function(e){

        var topLeftContentVar = tinymce.get("bottomRightTextArea").getContent();

        $(".bottomRightContent").html(topLeftContentVar);
        $("#bottomRightPageContent").val(topLeftContentVar);


        //jQuery.noConflict();

        var string = $('#bottomRightForm').serialize() + '&page_id=' + page_id;

		// AJAX Code To Submit Form.
		    $.ajax({
		        type: "POST",
		        url: "addPanel.php",
		        data: string,
		        cache: false,
		        success: function(response){
		          //console.log(JSON.stringify(response));

		          $('#bottomRightModal').modal('hide');
		          $('.modal-backdrop').remove();
		        }
		    });


        return false;

    });

});
</script>


<!-- Activate Page ON SAVE -->
<script type="text/javascript">
    $(document).ready(function(){

    $("#setActivePage").on("click", function(e){

        var string = '&page_id=' + page_id;

		// AJAX Code To Submit Form.
		    $.ajax({
		        type: "POST",
		        url: "savePage.php",
		        data: string,
		        cache: false,
		        success: function(response){
		        	window.location.href = "/pages.php";
		        }
		    });

        return false;

    });

});
</script>

<script src="tinyMCE/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
  tinymce.init({
    selector: '#leftHalfTextArea',
	height: 500,
	
  theme: 'modern',
  plugins: 'print preview fullpage paste searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools media  link contextmenu colorpicker textpattern help',
  toolbar1: 'preview | media | formatselect | bold italic strikethrough forecolor backcolor fontsizeselect fontselect | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  plugin_preview_width:800,
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  
  	image_class_list:[
  	{title: 'Responsive', value: 'img-responsive'}
  	],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ],
  
  });
</script>
<script type="text/javascript">
  tinymce.init({
    selector: '#rightHalfTextArea',
	height: 500,
	
  theme: 'modern',
  plugins: 'print preview fullpage paste searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools media  link contextmenu colorpicker textpattern help',
  toolbar1: 'preview | media | formatselect | bold italic strikethrough forecolor backcolor fontsizeselect fontselect | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  plugin_preview_width:800,
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
  });
</script>

<script type="text/javascript">
  tinymce.init({
    selector: '#fullTextArea',
	height: 500,
	
  theme: 'modern',
  plugins: 'print preview fullpage paste searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools media  link contextmenu colorpicker textpattern help',
  toolbar1: 'preview | media | formatselect | bold italic strikethrough forecolor backcolor fontsizeselect fontselect | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  plugin_preview_width:800,
  image_advtab: true,
  link_list: [
    {title:'Catnapper Power Recline', value:'<iframe width="100%" height="100%" src="https://www.youtube.com/embed/Qie7eGuVziU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'},
    {title:'Catnapper Italian Leather', value:'https://www.youtube.com/watch?v=9szFSbzoNqQ'},
    {title:'Jackson Advertising', value:'https://www.youtube.com/watch?v=98BHMIeY_-o'},
    {title:'Steel Tech Framing',value:'https://www.youtube.com/watch?v=Lh9hA_3S8l4&t=37s'},
    {title:'Catnapper Power Headrest',value:'https://www.youtube.com/watch?v=RhimfhVFJ8E'},
    {title:'Jackson Catnapper',value:'https://www.youtube.com/watch?v=EkRMED3YSnQ'}
    ],
  //   media_url_resolver: function (data, resolve/*, reject*/) {
  //   if (data.url.indexOf('Youtube') !== -1) {
  //     var embedHtml = '<iframe  width="100%" height="100%" src="' + data.url +
  //     '"></iframe>';
  //     resolve({html: embedHtml});
  //   } else {
  //     resolve({html: ''});
  //   }
  // },
  external_link_list_url : "tinyMCE/js/tinymce/plugins/lists/externallinks.js",
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ],
  });
</script>

<script type="text/javascript">
  tinymce.init({
    selector: '#topLeftTextArea',
	height: 500,
	
  theme: 'modern',
  plugins: 'print preview fullpage paste searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools media  link contextmenu colorpicker textpattern help',
  toolbar1: 'preview | media | formatselect | bold italic strikethrough forecolor backcolor fontsizeselect fontselect | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  plugin_preview_width:800,
  image_advtab: true,
  link_list: [
    {title:'Catnapper Power Recline', value:'https://www.youtube.com/watch?v=KczDCB4FQaE'},
    {title:'Catnapper Italian Leather', value:'https://www.youtube.com/watch?v=9szFSbzoNqQ'},
    {title:'Jackson Advertising', value:'https://www.youtube.com/watch?v=98BHMIeY_-o'},
    {title:'Steel Tech Framing',value:'https://www.youtube.com/watch?v=Lh9hA_3S8l4&t=37s'},
    {title:'Catnapper Power Headrest',value:'https://www.youtube.com/watch?v=RhimfhVFJ8E'},
    {title:'Jackson Catnapper',value:'https://www.youtube.com/watch?v=EkRMED3YSnQ'}
    ],
  external_link_list_url : "tinyMCE/js/tinymce/plugins/lists/externallinks.js",
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ],

  });
</script>

<script type="text/javascript">
  tinymce.init({
    selector: '#topRightTextArea',
	height: 500,
	
  theme: 'modern',
  plugins: 'print preview fullpage paste searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools media  link contextmenu colorpicker textpattern help',
  toolbar1: 'preview | media | formatselect | bold italic strikethrough forecolor backcolor fontsizeselect fontselect | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  plugin_preview_width:800,
  image_advtab: true,
  link_list: [
    {title:'Catnapper Power Recline', value:'https://www.youtube.com/watch?v=KczDCB4FQaE'},
    {title:'Catnapper Italian Leather', value:'https://www.youtube.com/watch?v=9szFSbzoNqQ'},
    {title:'Jackson Advertising', value:'https://www.youtube.com/watch?v=98BHMIeY_-o'},
    {title:'Steel Tech Framing',value:'https://www.youtube.com/watch?v=Lh9hA_3S8l4&t=37s'},
    {title:'Catnapper Power Headrest',value:'https://www.youtube.com/watch?v=RhimfhVFJ8E'},
    {title:'Jackson Catnapper',value:'https://www.youtube.com/watch?v=EkRMED3YSnQ'}
    ],
  external_link_list_url : "tinyMCE/js/tinymce/plugins/lists/externallinks.js",
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ],

  });
</script>

<script type="text/javascript">
  tinymce.init({
    selector: '#bottomLeftTextArea',
	height: 500,
	
  theme: 'modern',
  plugins: 'print preview fullpage paste searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools media  link contextmenu colorpicker textpattern help',
  toolbar1: 'preview | media | formatselect | bold italic strikethrough forecolor backcolor fontsizeselect fontselect | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  plugin_preview_width:800,
  image_advtab: true,
  link_list: [
    {title:'Catnapper Power Recline', value:'https://www.youtube.com/watch?v=KczDCB4FQaE'},
    {title:'Catnapper Italian Leather', value:'https://www.youtube.com/watch?v=9szFSbzoNqQ'},
    {title:'Jackson Advertising', value:'https://www.youtube.com/watch?v=98BHMIeY_-o'},
    {title:'Steel Tech Framing',value:'https://www.youtube.com/watch?v=Lh9hA_3S8l4&t=37s'},
    {title:'Catnapper Power Headrest',value:'https://www.youtube.com/watch?v=RhimfhVFJ8E'},
    {title:'Jackson Catnapper',value:'https://www.youtube.com/watch?v=EkRMED3YSnQ'}
    ],
  external_link_list_url : "tinyMCE/js/tinymce/plugins/lists/externallinks.js",
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ],

  });
</script>

<script type="text/javascript">
  tinymce.init({
    selector: '#bottomRightTextArea',
	height: 500,
	
  theme: 'modern',
  plugins: 'print preview fullpage paste searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools media  link contextmenu colorpicker textpattern help',
  toolbar1: 'preview | media | formatselect | bold italic strikethrough forecolor backcolor fontsizeselect fontselect | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  plugin_preview_width:800,
  image_advtab: true,
  link_list: [
    {title:'Catnapper Power Recline', value:'https://www.youtube.com/watch?v=KczDCB4FQaE'},
    {title:'Catnapper Italian Leather', value:'https://www.youtube.com/watch?v=9szFSbzoNqQ'},
    {title:'Jackson Advertising', value:'https://www.youtube.com/watch?v=98BHMIeY_-o'},
    {title:'Steel Tech Framing',value:'https://www.youtube.com/watch?v=Lh9hA_3S8l4&t=37s'},
    {title:'Catnapper Power Headrest',value:'https://www.youtube.com/watch?v=RhimfhVFJ8E'},
    {title:'Jackson Catnapper',value:'https://www.youtube.com/watch?v=EkRMED3YSnQ'}
    ],
  external_link_list_url : "tinyMCE/js/tinymce/plugins/lists/externallinks.js",
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ],

  });
</script>

<script type="text/javascript">
	$(document).on('focusin', function(e) {
    if ($(event.target).closest(".mce-window").length) {
        e.stopImmediatePropagation();
    }
});
</script>


<script type="text/javascript">
$(function () {
    $('.datepick').each(function(){
      $(this).datetimepicker({
        icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
    });
    });
});
</script>

<script type="text/javascript">
var $modalLeft = $('#leftFiftyModal').modal({
    show: false
});
$('.leftFifty').on('click', function() {
    $modalLeft.modal('show');
});

var $modalRight = $('#rightFiftyModal').modal({
    show: false
});
$('.rightFifty').on('click', function() {
    $modalRight.modal('show');
});

var $modalTopRight = $('#topRightModal').modal({
    show: false
});
$('.topRight').on('click', function() {
    $modalTopRight.modal('show');
});

var $modalTopLeft = $('#topLeftModal').modal({
    show: false
});
$('.topLeft').on('click', function() {
    $modalTopLeft.modal('show');
});

var $modalBottomLeft = $('#bottomLeftModal').modal({
    show: false
});
$('.bottomLeft').on('click', function() {
    $modalBottomLeft.modal('show');
});

var $modalBottomRight = $('#bottomRightModal').modal({
    show: false
});
$('.bottomRight').on('click', function() {
    $modalBottomRight.modal('show');
});


var $modalFull = $('#fullModal').modal({
    show: false
});
$('.fullWidth').on('click', function() {
    $modalFull.modal('show');
});

</script>

<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>

<script src="js/clock.js"></script>

<script type="text/javascript">
 	$('.marquee').marquee({
 		duration: 30000,
 		gap:30,
 	});
</script>

<!-- Apply Background image and save as panel type -->
<script type="text/javascript">
$('#exampleModal .modal-body img').click(function() {
  const src = $(this).attr("src");
  $('.my-container > .middle').css("background-image", `url(${src})`);
  $('#bgContent').val(src);

  var string = $('#bgForm').serialize() + '&page_id=' + page_id;
  console.log(string);

		// AJAX Code To Submit Form.
		    $.ajax({
		        type: "POST",
		        url: "addBackgroundImage.php",
		        data: string,
		        cache: false,
		        success: function(response){
		          console.log(JSON.stringify(response));

		          jQuery.noConflict();
				  $('#exampleModal').modal('hide');
				  $('.modal-backdrop').remove();
		        }
		    });


        return false;

});
</script>

<script type="text/javascript">
const availableSlides = document.querySelectorAll(".w3-content .mySlides");//Renamed
let slideIndex = 0; // Using 0 instead of 1 (for consistency w/ JS arrays)
showDivs();

function plusDivs(delta) { // Almost identical to your original function
  slideIndex += delta;
  showDivs();  
}

function wrap(tentative, max) { //This is where the magic happens
  let actualIndex;
  // If tentative index is too high/low, resumes counting from the bottom/top
  if (tentative > max) { actualIndex = tentative - (max + 1); }
  else if (tentative < 0) { actualIndex = tentative + (max + 1); }
  else { actualIndex = tentative; }
  return actualIndex;
}

function showDivs() { // Takes no args (acts on global slideIndex instead)
  // Hides all slides
  for (let i = 0; i < availableSlides.length; i++) {
    availableSlides[i].style.display = "none";
  }
  // Shows thatMany slides, starting from slideIndex
  const thatMany = 4; // Sets the number of slides to display
  // Calls wrap to keep slideIndex from being out of range
  slideIndex = wrap(slideIndex, availableSlides.length - 1);
  // Calls wrap on thatMany indexes and displays the resulting slides
  for (let j = 0; j < thatMany; j++) {
    let tentativeIndex = slideIndex + j;
    let maxIndex = availableSlides.length - 1;
    availableSlides[wrap(tentativeIndex, maxIndex)].style.display = "inline-block";
  }
}
</script>

<!-- <script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.querySelectorAll(".w3-content .mySlides");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script> -->

<?php endblock() ?>

</body>
</html>