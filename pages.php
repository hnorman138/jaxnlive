<?php require_once 'getPages.php'; 
   
?>


<!DOCTYPE html>
<html>
<head>
  <title>Page Templates</title>

  <style type="text/css" media="all">
  @import "style.css";
  </style>

  <!--Bootstrap CSS-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<body>

<?php include("menu.php");?>

<?php startblock('article') ?>
<div class="container-fluid" style="margin-bottom: 20px;">
<div class="row">
  <div class="col-lg-12">
    <div class="page-header"><h2>Pages</h2></div>
    <hr>
    <div class="row">
      <?php $count = 0;?>
      <?php foreach($result as $page): ?>
      <div class="col-lg-3 col-sm-6 d-flex" style="padding-bottom: 20px;">
      <div class="card text-center flex-fill">
      <h2><?php echo $page["title"] ?></h2> 
      <p><?php echo $page["Name"]?></p>
      <p>Duration: <?php echo $page["duration"]?> seconds</p>
      <p>Position: <?php echo $page["position"]?> <a href="" data-toggle="modal" data-target="#changePosition<?php echo $count; ?>">Change</a></p>
      <a target="_blank" href="showpage.php?pageid=<?php echo $page['id'] ?>">View Page</a>
      <!-- <a href="">Edit Page</a> -->
      <a href="" data-toggle="modal" data-target="#deletePage<?php echo $count; ?>">Delete Page</a>
      <a href="" data-toggle="modal" data-target="#changeDuration<?php echo $count; ?>" >Change Duration</a>
      <a href="" data-toggle="modal" data-target="#changeImage<?php echo $count; ?>" >Change Background Image</a>
      <a href="assignToAllDisplays.php?pageID=<?php echo $page['id']?>">Assign To All Displays</a>
      </div>
      </div>

      <!--Modal for changing duration-->
      <div class="modal fade" id="changeDuration<?php echo $count; ?>" tabindex="-1" role="dialog" aria-labelledby="changeDurationLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="changeDurationLabel">Enter new page duration:</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="changeDuration.php" method="post">
                <label>New Duration (in seconds):</label>
                <input type="text" name="duration">
                <input type="hidden" name="pageID" value="<?php echo $page['id']?>">

                <input type="submit" value="Update">
              </form>
            </div>
          </div>
        </div>
      </div>

      <!--Modal for changing duration-->
      <div class="modal fade" id="changePosition<?php echo $count; ?>" tabindex="-1" role="dialog" aria-labelledby="changePositionLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="changePositionLabel">Enter new position(1,5,10,etc.):</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="changePosition.php" method="post">
                
                <input type="text" name="position">
                <input type="hidden" name="pageID" value="<?php echo $page['id']?>">

                <input type="submit" value="Update">
              </form>
            </div>
          </div>
        </div>
      </div>

      <!--Modal for changing background IMage-->
      <div class="modal fade" id="changeImage<?php echo $count; ?>" tabindex="-1" role="dialog" aria-labelledby="changeImageLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="changeImageLabel">Choose new image:</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
                <?php foreach($imageResult as $im): ?>
                  <?php if($im['type'] == 'background' ) {?>
                    <img class="backgroundImage" style="width: 200px; height: 200px;" src="<?php echo $im['url'] ?>">
                  <?php } ?>
                <?php endforeach ?>

                <input type="hidden" name="pageID" value="<?php echo $page['id']?>">

              </form>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal to confirm page deletion -->
      <div class="modal fade" id="deletePage<?php echo $count; ?>" tabindex="-1" role="dialog" aria-labelledby="deletePageLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="deletePageLabel">Are you sure you want to delete this page?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <!-- <form method="post"> -->
              <!-- <input type="text" name="confirmDelete"> -->
              <?php //echo $page['id'];?>
              <a href="deletePage.php?deleteID=<?php echo $page['id']?>" class="btn btn-primary" role="button">Delete</a>
              <button class="btn btn-secondary" role="button" data-dismiss="modal">Cancel</button>
              <!-- </form> -->
            </div>

          </div>
        </div>
      </div>
      <?php $count++;?>
      <?php endforeach?>
    </div>
  </div>
</div>
</div>


<script type="text/javascript">
  $('img.backgroundImage').on('click', function() {
    
    var url = this.src;
    var page = $(this).closest(".modal").find("input[name='pageID']").val();
    //console.log(url);

    var formData = {
      'url': url,
      'pageID': page
    }; 
    console.log(formData);

    $.ajax({
      url:"changeImage.php",
      type: "POST",
      data: formData,
      dataType: 'json',
      success: function(d){
             $('.modal').modal('hide');
      }
    });

    $('.modal').modal('hide');
  })
</script>

<?php endblock() ?>

</body>
</html>