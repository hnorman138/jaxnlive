<?php require_once 'getPages.php'; ?>


<!DOCTYPE html>
<html>
<head>
  <title>News Tickers</title>

  <style type="text/css" media="all">
  @import "style.css";
  </style>

  <!--Bootstrap CSS-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<body>

<?php include("menu.php");?>

<?php startblock('article') ?>
<div class="container-fluid" style="margin-bottom: 20px;">
	<div class="row">
	    <div class="col-lg-12">
		    <div class="page-header"><h2>News Tickers</h2></div>
		    <hr>
		    <div class="row">
		    	<?php foreach($tickerDisplays as $key => $ticker):?>
		    		<div class="col-lg-3 col-sm-6 d-flex" style="padding-bottom: 20px;">
			          <div style="padding-bottom:20px;" class="card text-center flex-fill">
			            <h4 class="card-title"><?php echo $ticker['display'] ?> - <?php echo $ticker['locationName']?></h4>  
			            <hr>
			            <ul class="list-group">
			            	<?php foreach ( $ticker['@content'] as $contentID => $content ): ?>
			            	<li class="list-group-item disabled list-group-flush border-0" ><?php echo $content['content']?> | <a href="deleteTicker.php?deleteID=<?php echo $content['contentID']?>">Delete</a></li>
				            <?php endforeach;?>
			            </ul>
			            <h6 class="card-subtitle mb-2 text-muted" style="margin-top:50px; ">Add more Ticker Items</h6>
			            <form id="Items<?php echo $ticker['ticker']?>" class="tickerform" method="post">  
					        
						        <label id="ItemLabel">Item 1: </label>
						        <input type="text" name="Items[]" required><br/>
								<button type="button" class="moreItems_add">+</button>
						    
						    	<input type="hidden" name="tickerID" id="tickerID" class="tickerIdClass" value="<?php echo $ticker['ticker'] ?>">
						        <input type="submit" name="saveTickerItems" value="Save Ticker Items">  
					    </form>
			          </div>
			        </div>
		    	<?php endforeach;?>
		    </div>
		</div>
	</div>
</div>



<!-- ticker modal JS -->
<script type="text/javascript">
	
$("button.moreItems_add").on("click", function(e) {
var tickerID = $(this).closest('form').find('.tickerIdClass').val();
  var numItems = $("input[type='text']", $(this).closest("form")).length;
  if (numItems < 10) {
    var html = '<label class="ItemLabel">Item ' + (numItems + 1) + ': </label>';
    html += '<input type="text" name="Items[]"/><br/>';
    $(this).before(html);
    console.log(tickerID);
  }
});

</script>

<script type="text/javascript">
$(".tickerform").submit(function(e) {
	e.preventDefault();
	var data = $(this).serializeArray();
	console.log(data);

$.ajax({
       type: "POST",
       url: "addticker.php",
       data: data, // serializes the form's elements.
       success: function(data)
       {
           //alert(data); // show response from the php script.
           location.reload();
       }
     });

});

</script> 

<?php endblock() ?>

</body>
</html>