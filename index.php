<?php include 'getPages.php'?>



<!DOCTYPE html>
<html>
<head>
  <title>JaxnLive - Home</title>
  <style type="text/css" media="all">
    @import "style.css";
  </style>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <link href="https://rawgit.com/tempusdominus/bootstrap-4/master/build/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="src/ytv.css" type="text/css" rel="stylesheet" />

  <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="instafeed.js-master/instafeed.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.0/moment-with-locales.js"></script>
  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="https://rawgit.com/tempusdominus/bootstrap-4/master/build/js/tempusdominus-bootstrap-4.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
  <script src="src/ytv.js" type="text/javascript"></script>
  
</head>

<body>

  <?php include("menu.php");?>

  <?php startblock('article') ?>

<?php 

?>

<!-- CONTAINER -->
<div class="container-fluid" style="margin-bottom: 20px;">
  <!-- FIRST ROW -->
  <div class="row" style="width: 100%; padding-bottom: 20px;">
    <div class="col-sm-12">
      <div class="page-header">
        <h2>Active Displays</h2>      
      </div>
      <div class="row">
        <?php foreach($displayNames as $key => $displayName):?>
        <div class="col-lg-3 col-sm-6 d-flex" style="padding-bottom: 20px;">
          <div style="padding-bottom:20px;" class="card text-center flex-fill">
            <h4 class="card-title"><?php echo $key ?></h4>
            <?php foreach($displayName as $key2 => $displayNameRoom): ?>
            <a href="displayPage.php?displayID=<?php echo $key2;?>"><p class="card-text"><?php echo $displayNameRoom ?></p></a>
            <?php endforeach; ?>
          </div>
        </div>
      <?php endforeach; ?>
      </div>
    </div>
    <!-- END FIRST ROW -->
  </div>

  <!-- Third Row (WIDGETS) -->
  <div class="row" style="padding-top: 20px; padding-bottom: 40px;">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-lg-4 col-md-12 col-sm-12">
          <div class="page-header">
            <h2>Pages Expiring Soon</h2>
          </div>
          <div class="card" id="expDisplays" style="margin-bottom:20px; padding:10px;">
            <?php foreach($expiredPages as $expiredPage): ?>
              <h6><?php echo $expiredPage['title'] ?> - <?php echo $expiredPage['location_name']?> (<?php echo $expiredPage['display_name']?>)<a href="#" data-toggle="modal" data-target="#modal_<?php echo $expiredPage['title']; ?>_<?php echo $expiredPage['location_name']?>">Extend Time</a></h6>

              <!-- Modal -->
              <div class="modal fade" id="modal_<?php echo $expiredPage['title']; ?>_<?php echo $expiredPage['location_name']?>" tabindex="-1" role="dialog" aria-labelledby="expirationModal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="expirationModal">Extend Page Expiration Date</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      <h6>Current Expiration: <?php echo $expiredPage['end_time'] ?></h6>
                      <br>
                      <h6>New Expiration: </h6>
                      <form class="updateTime" method="POST">
                        <input type="hidden" name="currentPageID" value="<?php echo $expiredPage['id']?>">
                    ﻿ ﻿   <div class="datepick input-group date" id="datetimepicker_<?php echo $expiredPage['id']?>" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker_<?php echo $expiredPage['id']?>" name="datePicker" />
                        <span class="input-group-addon" data-target="#datetimepicker_<?php echo $expiredPage['id']?>" data-toggle="datetimepicker">
                        <span class="fa fa-calendar"></span>
                        </span>﻿﻿
                        </div>﻿﻿﻿﻿
                      
                    </div>
                    <div class="modal-footer">
                      <input type="submit" name="Extend Date" class="extendDate">
                    </div>
                    </form>
                  </div>
                </div>
              </div>
              <!-- End of Modal -->

              
     
            <?php endforeach; ?>

            <script type="text/javascript">
              $(".extendDate").click(function(event){
                  event.preventDefault();
                  var ser = $(this).closest("form").serialize();
                  console.log(ser);
                  // AJAX Code To Submit Form.
                  $.ajax({
                      type: "POST",
                      url: "extendTime.php",
                      data: ser,
                      dataType: 'json',
                      cache: false,
                      success: function(response){
                          console.log(response);
                      }
                  });
              });﻿
            </script>
          </div>
        </div>
        <!-- WIDGET 2 -->
        <div class="col-lg-8 col-md-12 col-sm-12">
          <div class="page-header">
            <h2>Social Media</h2>      
          </div>
          <div class="card text-center">
            <div class="card-header">
              <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                  <a data-toggle="tab" class="nav-link active" href="#instagram">Instagram</a>
                </li>
                <!-- <li class="nav-item">
                  <a data-toggle="tab" class="nav-link" href="#youtube">YouTube</a>
                </li> -->
                <li class="nav-item">
                  <a data-toggle="tab" class="nav-link" href="#twitter">Twitter</a>
                </li>
                <li class="nav-item">
                  <a data-toggle="tab" class="nav-link" href="#facebook">Facebook</a>
                </li>
              </ul>
            </div>
            <div class="card-body">
              <div class="tab-content">
                <div id="instagram" class="tab-pane fade show active">
                  <!-- <div class="container"> -->
                    <div id="instafeed">

                    </div><span id="showMore" style="cursor: pointer;"> Show More </span>
                  <!-- </div> -->
                </div>
                <div id="youtube" class="tab-pane fade">
                    <div id="YourPlayerID"></div>
                </div>
                <div id="twitter" class="tab-pane fade">
                 <a class="twitter-timeline" data-width="400" data-height="500" data-theme="light" href="https://twitter.com/BuiltByJackson?ref_src=twsrc%5Etfw">Tweets by BuiltByJackson</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
                <div id="facebook" class="tab-pane fade">
                  <div class="fb-page" width="500" data-href="https://www.facebook.com/BuiltByJackson" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/BuiltByJackson"><a href="https://www.facebook.com/BuiltByJackson">Jackson</a></blockquote></div></div>
                </div>
                </div>
              </div>
            </div>
          </div>
          <!-- END WIDGET 2 -->
        </div>
        <!-- END HOUSING ROW -->
      </div>
      <!-- END WIDGET FW COLUMN -->
    </div>
    <!-- END THIRD ROW -->
  </div>
<!-- END CONTAINER -->
</div>

<?php endblock() ?>

<script type="text/javascript">
$(function () {
    $('.datepick').each(function(){
      $(this).datetimepicker({
       icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
    });
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  var feed = new Instafeed({
    get: 'user',
    userId: '5590008752',
    clientId: 'e228035bad48471a94244cc6305cf827',
    accessToken: '5590008752.e228035.07bcba1d14024e8c82081c50e59023e5',
    // template: '<a class="gallery-item" target="_blank" href="{{link}}"><img class="image_img" src="{{image}}" /><div class="caption_layer"><p class="instaCaption">{{model.likes.count}} Likes</p></div></a>',
    template: '<a target="_blank" href="{{link}}"><img src="{{image}}" /></a>',
    sortBy: 'most-recent',
    limit: '4',
    // template: '<div class="wrap"><img src="{{image}}"/><div class="caption">{{caption}}</div></div>',
    useHttp: true
    });

  $('#showMore').on('click', function() {
     $("#instafeed").fadeTo("slow", 0.33, function() {
        $(this).css('opacity','1');
        $(this).empty(); // remove this line if you want to keep previously loaded images on the page
        feed.next();
     })
});

  feed.run();
});


</script>

<script type="text/javascript">
  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=2691675043";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<script type="text/javascript">
  document.addEventListener("DOMContentLoaded", function(event) {
    var controller = new YTV('YourPlayerID', {
     channelId: 'UCRQUfWh2QIE1iFo8AlGeYOg'
    });
});
</script>



</body>
</html>